package uvsq.m1info.roguelike.agents;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import uvsq.m1info.roguelike.FieldOfView;
import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;

public class TestCreature {
	Creature player = null;
	Creature familiar = null;
	Creature goblin = null;
	Creature merchant = null;
	World world = null;
	Tile[][][] tiles = null;
	FieldOfView fov = null;
	StuffFactory factory = null;
	Item item = null;
	@Before
	public void init() {
		tiles = new Tile[4][1][1];
		tiles[0][0][0] = Tile.FLOOR;
		tiles[1][0][0] = Tile.FLOOR;
		tiles[2][0][0] = Tile.FLOOR;
		tiles[3][0][0] = Tile.FLOOR;
		world = new World(tiles);
		fov = new FieldOfView(world);
		factory = new StuffFactory(world);
		player = factory.newPlayer(new ArrayList<String>(), fov);
		goblin = factory.newAggressiveCreature("Goblin", 0, player);
		familiar = factory.newCreature("Familiar", 0);
		merchant = factory.newMerchant();
	}
	
	@Test
	public void testIsPlayer() {
		assertTrue(player.isPlayer());
	}
	
	@Test
	public void testInitName() {
		assertEquals("player", player.name());
	}
	@Test
	public void testInitGlyph() {
		assertEquals(player.glyph(), '@');
	}
	
	@Test
	public void testInitAttackValue() {
		assertEquals(10, player.attackValue());
	}
	
	@Test
	public void testInitDefenseValue() {
		assertEquals(5, player.defenseValue());
	}
	
	@Test
	public void testInitMaxHp() {
		assertEquals(200, player.maxHp());
	}
	
	@Test
	public void testInitHp() {
		assertEquals(200, player.hp());
	}
	
	@Test
	public void testModifyHp() {
		player.modifyHp(-2, "Test");
		assertEquals(player.hp(), 198);
	}
	
	@Test
	public void testIsAlive() {
		assertTrue(player.isAlive());
	}
	
	@Test
	public void testIsDead() {
		player.modifyHp(-player.maxHp(), "Test");
		assertFalse(player.isAlive());
	}
	
	@Test
	public void testAdditionalLives() {
		player.addLifes(1);
		player.modifyHp(-player.maxHp(), "Test");
		assertTrue(player.isAlive());
	}
	
	@Test
	public void testNotTamed() {
		assertFalse(familiar.isTamed());
	}
	
	@Test
	public void testTamedByPlayer() {
		familiar.setHp(1);
		player.meleeAttack(familiar);
		assertTrue(familiar.isTamed());
	}
	
	@Test
	public void testNotTamedByMonster() {
		familiar.setHp(1);
		goblin.meleeAttack(familiar);
		assertFalse(familiar.isAlive());
	}
	
	@Test
	public void testAttacker() {
		player.meleeAttack(familiar);
		assertTrue(familiar.attacker() == player);
	}
	
	@Test
	public void testSeveralAttackers() {
		player.meleeAttack(familiar);
		goblin.meleeAttack(familiar);
		assertTrue(familiar.attacker() == goblin);
	}
	
	@Test
	public void testLeaveCorpse() {
		goblin.setHp(1);
		int x = goblin.getX();
		int y = goblin.getY();
		player.meleeAttack(goblin);
		assertTrue(world.item(x, y, 0).name().contains("goblin corpse"));
	}
	
	@Test
	public void testGainXp() {
		int xp = player.xp();
		goblin.setHp(1);
		player.meleeAttack(goblin);
		assertTrue(player.xp() > xp);
	}
	
	@Test
	public void testGainMoney() {
		int money = player.money();
		goblin.setHp(1);
		player.meleeAttack(goblin);
		assertTrue(player.money() > money);
	}
	
	@Test
	public void testAddItemToInventory() {
		item = factory.newFood(0);
		player.setLocation(item.location());
		player.pickup();
		assertTrue(player.inventory().contains(item));
	}
	
	@Test
	public void testRemoveItemFromFloor() {
		item = factory.newFood(0);
		player.setLocation(item.location());
		player.pickup();
		assertTrue(world.item(player.getX(), player.getY(), 0) == null);
	}

	@Test
	public void testDropItemFromInventory() {
		item = factory.newFood(0);
		player.setLocation(item.location());
		player.pickup();
		player.drop(item);
		assertFalse(player.inventory().contains(item));
	}
	
	@Test
	public void testDropItemToTheFloor() {
		item = factory.newFood(0);
		player.setLocation(item.location());
		player.pickup();
		player.drop(item);
		assertTrue(world.item(player.getX(), player.getY(), 0) == item);
	}
	
	@Test
	public void testEquipSword() {
		item = StuffFactory.newSword();
		player.inventory().add(item);
		player.equip(item);
		assertTrue(player.weapon() == item);
	}
	
	@Test
	public void testEquipArmor() {
		item = StuffFactory.newRandomArmor();
		player.inventory().add(item);
		player.equip(item);
		assertTrue(player.armor() == item);
	}
	
	@Test
	public void testEquipPickaxe() {
		item = StuffFactory.newPickaxe();
		player.inventory().add(item);
		player.equip(item);
		assertTrue(player.pickaxe() == item);
	}
	
	@Test
	public void testEquipShovel() {
		item = StuffFactory.newShovel();
		player.inventory().add(item);
		player.equip(item);
		assertTrue(player.shovel() == item);
	}
	
	@Test
	public void testUnequipSword() {
		item = StuffFactory.newSword();
		player.inventory().add(item);
		player.equip(item);
		player.unequip(item);
		assertTrue(player.weapon() == null);
	}
	
	@Test
	public void testDropEquippedSword() {
		item = StuffFactory.newSword();
		player.inventory().add(item);
		player.equip(item);
		player.drop(item);
		assertTrue(player.weapon() == null);
	}
	
	@Test
	public void testGetHungry() {
		int food = player.food();
		player.modifyFood(2);
		assertEquals(food, player.food() - 2);
	}
	
	@Test
	public void testEatApple() {
		int food = player.food();
		item = StuffFactory.newFruit();
		player.eat(item);
		assertTrue(food < player.food());
	}
	
	@Test
	public void testEatenApple() {
		item = StuffFactory.newFruit();
		player.inventory().add(item);
		player.eat(item);
		assertTrue(!player.inventory().contains(item));
	}
	
	@Test
	public void testMerchantHasKey() {
		merchant.sellItem(player, 1);
		assertTrue(merchant.inventory().get(0).name().contains("key"));
	}
	
	@Test
	public void testDontSellItem() {
		merchant.sellItem(player, 0);
		assertTrue(player.key() == null);
	}
	
	@Test
	public void testSellItemInventory() {
		player.setMoney(100);
		merchant.sellItem(player, 0);
		assertTrue(player.key() != null);
	}
	
	@Test
	public void testSellItemMoney() {
		player.setMoney(102);
		merchant.sellItem(player, 0);
		assertEquals(player.money(), 2);
	}
	
	public void testSellItemNewLovation() {
		player.setMoney(102);
		merchant.sellItem(player, 0);
		assertTrue(player.key().location() == player);
	}
	
}
