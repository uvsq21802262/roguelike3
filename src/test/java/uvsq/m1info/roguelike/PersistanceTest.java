package uvsq.m1info.roguelike;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import uvsq.m1info.roguelike.screens.PlayScreen;

public class PersistanceTest {

	/**
	 * Tests if directory exist else the directory is created
	 */
	@Test
	public void creatingDirectoryTest() {

		String dirPath = System.getProperty("user.dir")+File.separatorChar+"Test.txt";
		File dir = new File(dirPath);
		if(dir.exists())
		{
			File fichier =  new File(System.getProperty("user.dir")+File.separatorChar+"Test"+File.separatorChar+"Test.txt");
			if (fichier.exists())
			{
				fichier.delete();
			}
			dir.delete();
		}
		assertTrue(dir.mkdirs());
		
	}
	
	/**
	 * Tests the creation of the file
	 */
	@Test
	public void creatingFileTest() {

		String dirPath = System.getProperty("user.dir")+File.separatorChar+"Test";
		File dir = new File(dirPath);
		
		if(!dir.exists())
		{
			dir.mkdirs();
			File fichier =  new File(System.getProperty("user.dir")+File.separatorChar+"Test"+File.separatorChar+"Test.txt");
			try {
				assertTrue(fichier.createNewFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else 
		{
			File fichier =  new File(System.getProperty("user.dir")+File.separatorChar+"Test"+File.separatorChar+"Test.txt");
			try {
				if (fichier.exists())
				{
					fichier.delete();
				}
				assertTrue(fichier.createNewFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Tests the game save
	 */
	@Test
	public void saveTest() {

		File fichier =  new File(System.getProperty("user.dir")+File.separatorChar+"Test"+File.separatorChar+"Test.txt");
		PlayScreen p = new PlayScreen();
		
		try {
			Persistance.save(p);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(fichier.exists());
	}
	
	
	/**
	 * @throws ClassNotFoundException
	 * Try to resume the game 
	 */
	@Test
	public void loadGameTest() throws ClassNotFoundException {
		PlayScreen p;
		try {
			p = Persistance.resumeTest();
			assertTrue(p.getClass().isInstance(p));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
}
