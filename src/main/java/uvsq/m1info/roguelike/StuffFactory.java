
package uvsq.m1info.roguelike;

import java.io.Serializable;
import java.lang.reflect.Method;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uvsq.m1info.roguelike.agents.BatAi;
import uvsq.m1info.roguelike.agents.CannibalisticZombieAi;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.agents.FamiliarAi;
import uvsq.m1info.roguelike.agents.FungusAi;
import uvsq.m1info.roguelike.agents.GoblinAi;
import uvsq.m1info.roguelike.agents.MerchantAi;
import uvsq.m1info.roguelike.agents.MimicAi;
import uvsq.m1info.roguelike.agents.PlayerAi;
import uvsq.m1info.roguelike.agents.StrongZombieAi;
import uvsq.m1info.roguelike.agents.ZombieAi;
import uvsq.m1info.roguelike.agents.ZombieSummonerAi;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.World;

import asciiPanel.AsciiPanel;

public class StuffFactory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5270808718031777389L;
	private World world;
	private static Map<String, Color> potionColors;
	private static List<String> potionAppearances;

	public StuffFactory(World world) {
		this.world = world;
		setUpPotionAppearances();
	}

			
	public static Item newItem(String name, int x, int y, int z) {
		Item item = null;
		String methodName = "new"+ name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method= klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);
			
			Object[] args= new Object[0];
			item = (Item)method.invoke(null, args);
			
			item.setLocation(x, y, z);			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return item;
	}
	
	public Item newItem(String name, int z) {
		Item item = null;
		String methodName = "new"+name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method = klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);

			Object[] args = new Object[0];
			item = (Item)method.invoke(this, args);
			
			world.addAtEmptyLocation(item, z);	
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return item;
	}
	
	private static void setUpPotionAppearances() {
		potionColors = new HashMap<String, Color>();
		potionColors.put("red potion", AsciiPanel.brightRed);
		potionColors.put("yellow potion", AsciiPanel.brightYellow);
		potionColors.put("green potion", AsciiPanel.brightGreen);
		potionColors.put("cyan potion", AsciiPanel.brightCyan);
		potionColors.put("blue potion", AsciiPanel.brightBlue);
		potionColors.put("magenta potion", AsciiPanel.brightMagenta);
		potionColors.put("dark potion", AsciiPanel.brightBlack);
		potionColors.put("grey potion", AsciiPanel.white);
		potionColors.put("light potion", AsciiPanel.brightWhite);
		potionColors.put("purple potion", AsciiPanel.magenta);
		potionAppearances = new ArrayList<String>(potionColors.keySet());
		Collections.shuffle(potionAppearances);
	}
	
	public Creature newCreature(String name, int depth) {
		Creature creature = null;
		String methodName = "new"+name;
		
		Class<StuffFactory> klass = StuffFactory.class;
		Class[] parameterTypes = new Class[0];
		try {
			Method method = klass.getDeclaredMethod(methodName, parameterTypes);
			method.setAccessible(true);

			Object[] args = new Object[0];
			creature = (Creature)method.invoke(this, args);
			
			world.addAtEmptyLocation(creature, depth);	
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return creature;
	}
	//aggressive creatures
	public Creature newAggressiveCreature(String name, int depth, Creature target) {
		Creature creature = null;
		if (name.contains("ZombieSummoner")) 
			creature = newZombieSummoner(depth, target);
		else {
			String methodName = "new"+name;
			
			Class<StuffFactory> klass = StuffFactory.class;
			Class[] parameterTypes = new Class[1];
			parameterTypes[0] = Creature.class;
			try {
				Method method = klass.getDeclaredMethod(methodName, parameterTypes);
				method.setAccessible(true);
	
				Object[] args = new Object[1];
				args[0] = target;
				creature = (Creature)method.invoke(null, args);
			}
			catch(Exception ex) {
				ex.printStackTrace();
	
			}
		}
		creature.setWorld(world);
		world.addAtEmptyLocation(creature, depth);	
		if (name.contains("Goblin")) {
			creature.equip(newItem("RandomWeapon", depth));
			creature.equip(newItem("RandomArmor", depth));
		}
		return creature;
	}

	public Creature newPlayer(List<String> messages, FieldOfView fov) {
		Creature player = new Creature(world, '@', AsciiPanel.brightWhite, "player", 1000, 10, 5);
		world.addAtEmptyLocation(player, 0);
		new PlayerAi(player, messages, fov);
		return player;
	}
	
	public Creature newMerchant() {
		Creature merchant = new Creature(world, 'm', AsciiPanel.brightYellow, "merchant", 400, 30, 10);
		merchant.setMoney(100);
		merchant.setStockRoom(20);
		new MerchantAi(merchant);
		merchant.addItemToStock(newKey(), 100);
		merchant.addItemToStock(newFood(), 10);
		return merchant;
	}
	
	public Creature newNoobMerchant() {
		Creature merchant = newMerchant();
		merchant.addItemToStock(newTool(), 200);
		merchant.addItemToStock(newRandomArmor(), 100);
		merchant.addItemToStock(newRandomWeapon(), 100);
		return merchant;
	}

	public Creature newExpertMerchant() {
		Creature merchant = newMerchant();
		merchant.addItemToStock(StuffFactory.newRandomRelic(), 500);
		merchant.addItemToStock(StuffFactory.newRandomCursedRelic(), 500);
		merchant.addItemToStock(newRandomPotion(), 10);

		return merchant;
	}

	public Creature newFamiliar() {
		Creature pet = new Creature(world, 'a', AsciiPanel.red, "wild rat", 200, 20, 5);
		new FamiliarAi(pet);
		return pet;
	}

	public Creature selectNewMerchant(int depth) {
		if (depth < world.depth() - 1)
			return newCreature("NoobMerchant", depth);
		else
			return newCreature("ExpertMerchant", depth);
	}

	public Creature newFungus() {
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, "fungus", 10, 0, 0);
		new FungusAi(fungus, this);
		fungus.setMoney(5);
		return fungus;
	}

	public Creature newBat() {
		Creature bat = new Creature(world, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		new BatAi(bat);
		bat.setMoney(10);
		return bat;
	}
	
	public static Creature newSummonedBat() {
		Creature bat = new Creature(null, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		new BatAi(bat);
		bat.setMoney(10);
		return bat;
	}
	
	public static Creature newZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.white, "zombie", 30, 10, 10);
		new ZombieAi(zombie, player);
		zombie.setMoney(15);
		return zombie;
	}
	
	public static Creature newStrongZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.brightMagenta, "strong zombie", 120, 1, 5);
		new StrongZombieAi(zombie, player);
		zombie.setMoney(40);
		return zombie;
	}
	
	public static Creature newCannibalisticZombie(Creature player) {
		Creature zombie = new Creature(null, 'z', AsciiPanel.brightYellow, "cannibalistic zombie", 10, 1, 1);
		new CannibalisticZombieAi(zombie, player);
		zombie.setMoney(20);
		return zombie;
	}

	public static Creature newLesserZombie( Creature player) {
		Creature zombie = new Creature(null, 'z', Color.GRAY, "lesser zombie", 1, 1, 1);
		new ZombieAi(zombie, player);
		zombie.setMoney(1);
		return zombie;
	}
	
	public static Creature newLesserZombie(int depth, Creature player) {
		Creature zombie = new Creature(null, 'z', Color.GRAY, "lesser zombie", 1, 1, 1);
		new ZombieAi(zombie, player);
		zombie.setMoney(1);
		return zombie;
	}
	
	static public Creature newZombieFromCorpse(Creature player, Item corpse) {
		Creature zombie = null;
		if (corpse.name().contains("strong")) {
			zombie = StuffFactory.newStrongZombie(player);
		}else if (corpse.name().contains("lesser")) {
			return null;
		} else if (corpse.name().contains("cannibalistic")) {
			zombie = StuffFactory.newCannibalisticZombie(player);
		}
		else {
			zombie = StuffFactory.newZombie(player);
		}
		zombie.setLocation(corpse.getX(), corpse.getY(), corpse.getZ());
		return zombie;
	}
	
	private Creature newZombieSummoner(int depth, Creature player) {
		Creature zombie = new Creature(null, 'n', Color.DARK_GRAY, "necromancer", 100, 1, 20);
		new ZombieSummonerAi(zombie, player, this);
		zombie.setMoney(100);
		zombie.inventory().add(newVictoryItem());
		return zombie;
	}
	
	public static Creature newGoblin(Creature player) {
		Creature goblin = new Creature(null, 'g', AsciiPanel.brightGreen, "goblin", 66, 15, 5);
		new GoblinAi(goblin, player);
		goblin.setMoney(25);
		return goblin;
	}
	
	public static Creature newMimic(int x, int y, int z, Creature player) {
		Creature mimic = new Creature(null, 'c', AsciiPanel.yellow, "mimic", 200, 15, 5);
		new MimicAi(mimic, player);
		mimic.setLocation(x, y, z);
		if (Math.random() < 0.4)
			mimic.inventory().add(newRandomCursedRelic());
		mimic.inventory().add(newFood());
		mimic.setMoney(75);
		return mimic;
	}
	
	public static Item newRock() {
		Item rock = new Item(',', AsciiPanel.yellow, "rock");
		return rock;
	}
	
	private Item newVictoryItem() {
		Item item = new Item('*', AsciiPanel.brightWhite, "exit key");
		return item;
	}

	public Item newTorch(int depth) {
		Item torch = new Item('i', AsciiPanel.brightYellow, "torchlight");
		torch.modifyThrownAttackValue(5);
		torch.setDurationValue(40);
		world.addAtEmptyLocation(torch, depth);
		return torch;
	}

	public static Item newPickaxe() {
		Item pickaxe = new Item('p', AsciiPanel.red, "pickaxe");
		pickaxe.setMiningValue(2);
		pickaxe.setDurationValue(15);
		return pickaxe;
	}

	public static Item newShovel() {
		Item shovel = new Item('s', AsciiPanel.red, "shovel");
		shovel.setDurationValue(3);
		return shovel;
	}

	public Item newKey() {
		Item item = new Item('k', AsciiPanel.brightYellow, "key");
		return item;
	}
	
	public static Item newFruit() {
		Item item = new Item('%', AsciiPanel.brightRed, "apple");
		item.modifyFoodValue(100);
		return item;
	}
	
	public static Item newBread() {
		Item item = new Item('%', AsciiPanel.yellow, "bread");
		item.modifyFoodValue(400);
		return item;
	}
	
	public Item newFood(int depth) {
		switch((int)(Math.random()) * 2) {
		case 0 : return newItem("Fruit", depth);
		default : return newItem("Bread", depth);
		}
	}
	
	public static Item newFood() {
		switch((int)(Math.random()) * 2) {
		case 0 : return newFruit();
		default : return newBread();
		}
	}
	
	public static Item newTool() {
		if (Math.random() < 0.8) 
			return newPickaxe();
		return newShovel();
	}

	public static Item newDagger() {
		Item item = new Item(')', AsciiPanel.white, "dagger");
		item.modifyAttackValue(5);
		item.modifyThrownAttackValue(5);
		item.setDurationValue(30);
		return item;
	}

	public static Item newSword() {
		Item item = new Item(')', AsciiPanel.brightWhite, "sword");
		item.modifyAttackValue(10);
		item.modifyThrownAttackValue(3);
		item.setDurationValue(30);
		return item;
	}

	public static Item newStaff() {
		Item item = new Item(')', AsciiPanel.yellow, "staff");
		item.modifyAttackValue(5);
		item.modifyDefenseValue(3);
		item.modifyThrownAttackValue(3);
		item.setDurationValue(30);
		return item;
	}

	public static Item newBow() {
		Item item = new Item(')', AsciiPanel.yellow, "bow");
		item.modifyAttackValue(1);
		item.modifyRangedAttackValue(5);
		item.setDurationValue(30);
		return item;
	}

	public static Item newEdibleWeapon() {
		Item item = new Item(')', AsciiPanel.yellow, "baguette");
		item.modifyAttackValue(3);
		item.modifyFoodValue(100);
		item.setDurationValue(30);
		return item;
	}

	private static Item newLightArmor() {
		Item item = new Item('[', AsciiPanel.green, "tunic");
		item.modifyDefenseValue(2);
		item.setDurationValue(30);
		return item;
	}

	private static Item newMediumArmor() {
		Item item = new Item('[', AsciiPanel.white, "chainmail");
		item.modifyDefenseValue(4);
		item.setDurationValue(30);
		return item;
	}

	private static Item newHeavyArmor() {
		Item item = new Item('[', AsciiPanel.brightWhite, "platemail");
		item.modifyDefenseValue(6);
		item.setDurationValue(30);
		return item;
	}

	public static Item newRandomWeapon() {
		switch ((int) (Math.random() * 3)) {
		case 0 : return newDagger();
		case 1 : return newSword();
		case 2 : return newBow();
		default : return newStaff();
		}
	}

	public static Item newRandomArmor() {
		switch ((int) (Math.random() * 3)) {
		case 0 : return newLightArmor();
		case 1 : return newMediumArmor();
		default : return newHeavyArmor();
		}
	}


	private static Item newPotionOfHealth() {
		String appearance = potionAppearances.get(0);
		final Item item = new Item('!', potionColors.get(appearance), "health potion");
		item.setQuaffEffect(Effect.HEALTH);
		return item;
	}

	private static Item newPotionOfMana() {
		String appearance = potionAppearances.get(1);
		final Item item = new Item('!', potionColors.get(appearance), "mana potion");
		item.setQuaffEffect(Effect.MANA);
		return item;
	}

	private static Item newPotionOfSlowHealth() {
		String appearance = potionAppearances.get(2);
		final Item item = new Item('!', potionColors.get(appearance), "slow health potion");
		item.setQuaffEffect(Effect.SLOWHEALTH);
		return item;
	}

	private static Item newPotionOfPoison() {
		String appearance = potionAppearances.get(3);
		final Item item = new Item('!', potionColors.get(appearance), "poison potion");
		item.setQuaffEffect(Effect.POISON);
		return item;
	}

	private static Item newPotionOfWarrior() {
		String appearance = potionAppearances.get(4);
		final Item item = new Item('!', potionColors.get(appearance), "warrior's potion");
		item.setQuaffEffect(Effect.WARRIOR);
		return item;
	}

	private static Item newPotionOfArcher() {
		String appearance = potionAppearances.get(5);
		final Item item = new Item('!', potionColors.get(appearance), "archers potion");
		item.setQuaffEffect(Effect.ARCHER);
		return item;
	}

	private static Item newPotionOfExperience() {
		String appearance = potionAppearances.get(6);
		final Item item = new Item('!', potionColors.get(appearance), "experience potion");
		item.setQuaffEffect(Effect.XPBOOST);
		return item;
	}

	private static Item newPotionOfVision() {
		String appearance = potionAppearances.get(7);
		final Item item = new Item('!', potionColors.get(appearance), "vision potion");
		item.setQuaffEffect(Effect.VISION);
		return item;
	}

	public static Item newRandomPotion() {
		switch ((int) (Math.random() * 10)) {
		case 0:
			return newPotionOfHealth();
		case 1:
			return newPotionOfHealth();
		case 2:
			return newPotionOfMana();
		case 3:
			return newPotionOfMana();
		case 4:
			return newPotionOfSlowHealth();
		case 5:
			return newPotionOfPoison();
		case 6:
			return newPotionOfWarrior();
		case 7:
			return newPotionOfArcher();
		case 8:
			return newPotionOfVision();
		default:
			return newPotionOfExperience();
		}
	}


	private static Item newWhiteMagesSpellbook() {
		Item item = new Item('+', AsciiPanel.brightWhite, "white mage's spellbook");
		item.addWrittenSpell("minor heal", 4, Effect.MINORHEAL, true);
		item.addWrittenSpell("major heal", 8, Effect.MAJORHEAL, true);
		item.addWrittenSpell("slow heal", 12, Effect.SLOWHEAL, false);
		item.addWrittenSpell("inner strength", 16, Effect.INNERSTRENGTH, false);
		return item;
	}

	private static Item newBlueMagesSpellbook() {
		Item item = new Item('+', AsciiPanel.brightBlue, "blue mage's spellbook");
		item.addWrittenSpell("blood to mana", 1, Effect.BLOODTOMANA, true);
		item.addWrittenSpell("blink", 6, Effect.BLINK, false);
		item.addWrittenSpell("summon bats", 11, Effect.SUMMONBATS, false);
		item.addWrittenSpell("detect creatures", 16, Effect.DETECTCREATURES, false);
		return item;
	}

	public static Item newRandomSpellBook() {
		switch ((int) (Math.random() * 2)) {
		case 0:
			return newWhiteMagesSpellbook();
		default:
			return newBlueMagesSpellbook();
		}
	}

	public static Item newOrichalcumSword() {
		Item item = new Item(')', AsciiPanel.brightYellow, "orichalcum sword");
		item.modifyAttackValue(30);
		item.modifyThrownAttackValue(3);
		return item;
	}

	private static Item newSwordOfFortune() {
		Item item = new Item(')', AsciiPanel.brightYellow, "sword of fortune");
		item.modifyAttackValue(10);
		item.multiplyCoin(2);
		return item;
	}

	private static Item newArmorOfVitality() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of vitality");
		item.modifyDefenseValue(15);
		item.modifyAttackValue(1);
		item.setMaxHpBonus(20);
		return item;
	}

	private static Item newArmorOfWealth() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of wealth");
		item.modifyDefenseValue(8);
		item.multiplyCoin(2);
		return item;
	}

	private static Item newArmorOfImmortality() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of immortality");
		item.modifyDefenseValue(10);
		item.setAdditionalLives(1);
		return item;
	}

	public static Item newRandomRelic() {
		switch ((int) (Math.random() * 5)) {
		case 0:
			return newOrichalcumSword();
		case 1:
			return newArmorOfVitality();
		case 2:
			return newArmorOfImmortality();
		case 3:
			return newSwordOfFortune();
		default:
			return newArmorOfWealth();
		}
	}

	private static Item newArmorOfSickness() {
		Item item = new Item('[', AsciiPanel.brightYellow, "armor of Sickness");
		item.modifyDefenseValue(10);
		item.setMaxHpBonus(-20);
		return item;
	}

	private static Item newSwordOfMisfortune() {
		Item item = new Item(')', AsciiPanel.brightYellow, "sword of misfortune");
		item.modifyAttackValue(50);
		item.multiplyCoin(0.5);
		return item;
	}

	public static Item newRandomCursedRelic() {
		switch ((int) (Math.random() * 2)) {
		case 0:
			return newArmorOfSickness();
		default:
			return newSwordOfMisfortune();
		}
	}
}