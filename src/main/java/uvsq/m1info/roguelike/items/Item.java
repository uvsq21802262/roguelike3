package uvsq.m1info.roguelike.items;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uvsq.m1info.roguelike.Effect;
import uvsq.m1info.roguelike.Spell;
import uvsq.m1info.roguelike.map.Location;
import uvsq.m1info.roguelike.map.Point;

public class Item implements Location, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6960457382915534897L;
	private char glyph;
	public char glyph() { return glyph; }
	
	private Color color;
	public Color color() { return color; }

	private String name;
	public String name() { return name; }
	
	private int foodValue;
	public int foodValue() { return foodValue; }
	public void modifyFoodValue(int amount) { foodValue += amount; }

	private int attackValue;
	public int attackValue() { return attackValue; }
	public void modifyAttackValue(int amount) { attackValue += amount; }

	private int defenseValue;
	public int defenseValue() { return defenseValue; }
	public void modifyDefenseValue(int amount) { defenseValue += amount; }

	private int thrownAttackValue;
	public int thrownAttackValue() { return thrownAttackValue; }
	public void modifyThrownAttackValue(int amount) { thrownAttackValue += amount; }

	private int rangedAttackValue;
	public int rangedAttackValue() { return rangedAttackValue; }
	public void modifyRangedAttackValue(int amount) { rangedAttackValue += amount; }
	
	private int maxHpBonus;
	public int maxHpBonus() { return maxHpBonus; }
	public void setMaxHpBonus(int value) { maxHpBonus = value; }
	
	private double coinMultiplicator;
	public double coinMultiplicator() { return coinMultiplicator; }
	public void multiplyCoin(double value) { coinMultiplicator *= value; }

	private int additionalLives;
	public int additionalLives() { return additionalLives; }
	public void setAdditionalLives(int value) { additionalLives = value; }
	
	private Effect quaffEffect;
	public Effect quaffEffect() { return quaffEffect; }
	public void setQuaffEffect(Effect effect) { this.quaffEffect = effect; }
	
	private List<Spell> writtenSpells;
	public List<Spell> writtenSpells() { 
		return writtenSpells; 
	}
	public void addWrittenSpell(String name, int manaCost, Effect effect, boolean target){
		writtenSpells.add(new Spell(name, manaCost, effect, target));
	}
	
	private int diggingValue;
	public int getDiggingValue(){ return diggingValue; }
	public void setDiggingValue(int amount){ diggingValue = amount; }
	
	private int miningValue;
	public int miningValue(){ return miningValue; }
	public void setMiningValue(int amount){ miningValue = amount; }
	
	private int durationValue = 0;

	public void decreaseDurationValue() { 
		durationValue--; 
	}
	
	
	public void setDurationValue(int durationValue) { 
		this.durationValue = durationValue;
	}
	
	public int durationValue() { 
		return durationValue; 
	}
	
	public void resetDurationValue() { 
		durationValue = 10; 
	}
	
	public boolean isZombieCorpse() {
		return name.contains("zombie corpse");
	}

	public boolean canRevive(){
		if(isZombieCorpse()) 
			return durationValue < -1;
		return false;
	}
	
	public Item(){
		this.coinMultiplicator = 1;
		this.maxHpBonus = 0;
	}
	public Item(char glyph, Color color, String name){
		this();
		this.glyph = glyph;
		this.color = color;
		this.name = name;
		this.thrownAttackValue = 1;
		this.writtenSpells = new ArrayList<Spell>();
		
	}
	
	public String details() {
		String details = "";
		
		if (attackValue != 0)
			details += "  attack:" + attackValue;

		if (thrownAttackValue != 1)
			details += "  thrown:" + thrownAttackValue;
		
		if (rangedAttackValue > 0)
			details += "  ranged:" + rangedAttackValue;
		
		if (defenseValue != 0)
			details += "  defense:" + defenseValue;

		if (foodValue != 0)
			details += "  food:" + foodValue;
		
		return details;
	}

	public boolean isTorch() {
		return name == "torchlight";
	}
	
	public boolean isWeapon() {
		return (attackValue > 0 || rangedAttackValue > 0) && (attackValue + rangedAttackValue > defenseValue); 
	}
	
	public boolean isArmor(){
		return defenseValue > 0 && defenseValue > attackValue + rangedAttackValue;
	}
	
	public boolean isPickaxe() {
		return miningValue > 0;
	}
	
	public boolean isShovel() {
		return name.contains("shovel");
	}
	
	public boolean isBroken() {
		return durationValue < 0;
	}

	private Location location;
	@Override
	public void setLocation(int x, int y, int z) {
		location = new Point(x, y, z);
	}
	@Override
	public Location location() {
		return location;
	}
	@Override
	public void setLocation(Location location) {
		this.location = location;
		
	}
	@Override
	public int getX() {
		return location.getX();
	}
	@Override
	public int getY() {
		return location.getY();
	}
	@Override
	public int getZ() {
		return location.getZ();
	}
}
