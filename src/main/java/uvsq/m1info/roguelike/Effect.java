package uvsq.m1info.roguelike;

import java.io.Serializable;

import asciiPanel.AsciiPanel;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;

public enum Effect implements Serializable {
	HEALTH(1) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			if (creature.hp() == creature.maxHp())
				return;
			creature.modifyHp(15, "Killed by a health potion?");
			creature.doAction(item, "look healthier");
		}
	},
	MANA(1) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			if (creature.mana() == creature.maxMana())
				return;

			creature.modifyMana(10);
			creature.doAction(item, "look restored");
		}
	},
	SLOWHEALTH(100) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			creature.doAction(item, "look a little better");
		}
		
		public void update(Creature creature) {
			super.update(creature);
			creature.modifyHp(1, "Killed by a slow health potion?");
		}
	},
	WARRIOR(20) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			creature.modifyAttackValue(5);
			creature.modifyDefenseValue(5);
			creature.doAction(item, "look stronger");
		}

		public void end(Creature creature) {
			creature.modifyAttackValue(-5);
			creature.modifyDefenseValue(-5);
			creature.doAction("look less strong");
		}
	},
	ARCHER(20) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			creature.modifyVisionRadius(3);
			creature.doAction(item, "look more alert");
		}

		public void end(Creature creature) {
			creature.modifyVisionRadius(-3);
			creature.doAction("look less alert");
		}
	},
	XPBOOST(1) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			creature.doAction(item, "look more experienced");
			creature.modifyXp(creature.level() * 5);
		}
	},
	VISION(30) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			creature.addVisibility();
			creature.doAction(item, "look enlightened");
		}

		public void end(Creature creature) {
			creature.removevisibility();
		}
	},
	POISON(10) {
		public void start(Creature creature, Item item) {
			super.start(creature);
			if (creature.isPlayer()) 
				creature.setColor(AsciiPanel.brightGreen);
			creature.doAction(item, "look sick");
		}

		public void start(Creature creature) {
			super.start(creature);
			if (creature.isPlayer()) {
				creature.setColor(AsciiPanel.brightGreen);
				System.out.println("change"+creature.name());
			}
			creature.doAction("look sick");
		}
		
		public void update(Creature creature) {
			super.update(creature);
			creature.modifyHp(- creature.maxHp() / 30, "Died of poison.");
		}
		
		public void end(Creature creature) {
			if (creature.isPlayer()) {
				creature.setColor(AsciiPanel.brightWhite);

				System.out.println("end"+creature.name());
			}
		}
	},
	MINORHEAL(1) {
		public void start(Creature creature) {
			super.start(creature);
			if (creature.hp() == creature.maxHp())
				return;
			creature.modifyHp(20, "Killed by a minor heal spell?");
			creature.doAction("look healthier");
		}
	},
	
	MAJORHEAL(1) {
		public void start(Creature creature) {
			super.start(creature);
			if (creature.hp() == creature.maxHp())
				return;
			creature.modifyHp(50, "Killed by a major heal spell?");
			creature.doAction("look healthier");
		}
	},
	
	SLOWHEAL(50) {
		public void update(Creature creature) {
			super.update(creature);
			creature.modifyHp(2, "Killed by a slow heal spell?");
		}
	},
	
	INNERSTRENGTH(50) {
		public void start(Creature creature) {
			super.start(creature);
			creature.modifyAttackValue(2);
			creature.modifyDefenseValue(2);
			creature.modifyVisionRadius(1);
			creature.modifyRegenHpPer1000(10);
			creature.modifyRegenManaPer1000(-10);
			creature.doAction("seem to glow with inner strength");
		}

		public void update(Creature creature) {
			super.update(creature);
			if (Math.random() < 0.25)
				creature.modifyHp(1, "Killed by inner strength spell?");
		}

		public void end(Creature creature) {
			creature.modifyAttackValue(-2);
			creature.modifyDefenseValue(-2);
			creature.modifyVisionRadius(-1);
			creature.modifyRegenHpPer1000(-10);
			creature.modifyRegenManaPer1000(10);
		}
	},
	
	BLOODTOMANA(1) {
		public void start(Creature creature) {
			super.start(creature);
			int amount = Math.min(creature.hp() - 1, creature.maxMana() - creature.mana());
			creature.modifyHp(-amount, "Killed by a blood to mana spell.");
			creature.modifyMana(amount);
		}
	},
	
	BLINK(1) {
		public void start(Creature creature) {
			super.start(creature);
			creature.doAction("fade out");

			int mx = 0;
			int my = 0;

			do {
				mx = (int) (Math.random() * 11) - 5;
				my = (int) (Math.random() * 11) - 5;
			} while (!creature.canEnter(creature.x + mx, creature.y + my, creature.z)
					&& creature.canSee(creature.x + mx, creature.y + my, creature.z));

			creature.moveBy(mx, my, 0);

			creature.doAction("fade in");
		}
	},
	
	SUMMONBATS(1) {
		public void start(Creature creature) {
			super.start(creature);
			for (int ox = -1; ox < 2; ox++) {
				for (int oy = -1; oy < 2; oy++) {
					int nx = creature.x + ox;
					int ny = creature.y + oy;
					if (ox == 0 && oy == 0 || creature.creature(nx, ny, creature.z) != null)
						continue;
					creature.summonBat(nx, ny, creature.z);
				}
			}
		}
	},
	
	DETECTCREATURES(75) {
		public void start(Creature creature) {
			super.start(creature);
			creature.doAction("look far off into the distance");
			creature.modifyDetectCreatures(1);
		}

		public void end(Creature creature) {
			creature.modifyDetectCreatures(-1);
		}
	}; 
	
	public boolean isDone() { return timer < 1; }

	private Effect(int duration){
		this.duration = duration;
		this.timer = duration;
	}
	
	private Effect(Effect other){
		this.duration = other.duration; 
		this.timer = duration;
	}
	
	public void update(Creature creature){
		timer--;
	}
	private int timer;
	private int duration;
	
	public void start(Creature creature){
		this.timer = this.duration;
	}
	
	public void start(Creature creature, Item item){
		this.timer = this.duration;
	}
	public void end(Creature creature){
		
	}
}