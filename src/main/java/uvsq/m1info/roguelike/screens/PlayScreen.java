package uvsq.m1info.roguelike.screens;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;
import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.FieldOfView;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;
import uvsq.m1info.roguelike.map.WorldBuilder;
import uvsq.m1info.roguelike.screens.interaction.InteractScreen;
import uvsq.m1info.roguelike.screens.inventory.DropScreen;
import uvsq.m1info.roguelike.screens.inventory.EatScreen;
import uvsq.m1info.roguelike.screens.inventory.EquipScreen;
import uvsq.m1info.roguelike.screens.inventory.ExamineScreen;
import uvsq.m1info.roguelike.screens.inventory.QuaffScreen;
import uvsq.m1info.roguelike.screens.inventory.ReadScreen;
import uvsq.m1info.roguelike.screens.target.FireWeaponScreen;
import uvsq.m1info.roguelike.screens.target.LookScreen;

public class PlayScreen implements Screen, Serializable {
	/**
	 * this class is the main view in the game and allows you to play the game
	 */
	private static final long serialVersionUID = 4018250876682291169L;
	private World world;
	private Creature player;
	private int screenWidth;
	private int screenHeight;
	private List<String> messages;
	private FieldOfView fov;
	private Screen subscreen;
	
	public PlayScreen(){
		screenWidth = 80;
		screenHeight = 23;
		messages = new ArrayList<String>();
		createWorld();
		fov = new FieldOfView(world);
		
		StuffFactory factory = new StuffFactory(world);
		createCreatures(factory);
		createItems(factory);
	}

	private void createCreatures(StuffFactory factory){
		player = factory.newPlayer(messages, fov);
		
		for (int z = 0; z < world.depth(); z++) {
			for (int i = 0; i < 20; i++) {
				factory.newCreature("Bat", z);
			}
			for (int i = 0; i < 2 * z + 1; i++) {
				factory.newAggressiveCreature("Zombie", z, player);
				factory.newAggressiveCreature("Goblin", z, player);
			}
			factory.selectNewMerchant(z);
			if (z > 2) {
				factory.newAggressiveCreature("StrongZombie", z, player);
			}
			factory.newAggressiveCreature("ZombieSummoner",world.depth() - 1, player);
			for (int i = 0; i < 2 * z + 1; i++) {
				//if (z > 2)
					factory.newAggressiveCreature("CannibalisticZombie", z, player);
			}
		}

		factory.newCreature("Familiar",2);
		factory.newCreature("Familiar", world.depth() - 1);
	}

	private void createItems(StuffFactory factory) {
		for (int z = 0; z < world.depth(); z++){
			for (int i = 0; i < world.width() * world.height() / 50; i++){
				factory.newItem("Rock", z);
			}
			for (int i = 0; i < world.width() * world.height() / 700; i++){
				Item item;
				item = factory.newTorch(z);
				fov.update(item.getX(), item.getY(), item.getZ(), 3);
			}
			factory.newFood(z);
		}
	}
	
	private void createWorld(){
		world = new WorldBuilder(90, 32, 5)
					.makeCaves()
					.build();
	}
	public int getScrollX() { return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth)); }
	
	public int getScrollY() { return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight)); }
	
	@Override
	public void displayOutput(AsciiPanel terminal) {
		int left = getScrollX();
		int top = getScrollY(); 
		
		displayTiles(terminal, left, top);
		displayMessages(terminal, messages);
		
		String stats = String.format(" %3d/%3d hp   %d/%d mana   x%d   %8s   o:%d   (%d,%d,%d)", 
				player.hp(), player.maxHp(), player.mana(), player.maxMana(), 
				player.additionalLives(), hunger(), player.money(), player.x, player.y, player.z);
		terminal.write(stats, 1, 23);
		terminal.write(world.tile(player.x,player.y,player.z).details(),1,1);
		if (subscreen != null)
			subscreen.displayOutput(terminal);
	}
	
	private String hunger(){
		if (player.food() < player.maxFood() * 0.10)
			return "Starving";
		else if (player.food() < player.maxFood() * 0.25)
			return "Hungry";
		else if (player.food() > player.maxFood() * 0.90)
			return "Stuffed";
		else if (player.food() > player.maxFood() * 0.75)
			return "Full";
		else
			return "";
	}
	
	private void displayMessages(AsciiPanel terminal, List<String> messages) {
		int top = screenHeight - messages.size();
		for (int i = 0; i < messages.size(); i++){
			//bug a fixer en debut de jeu
			try{
				terminal.writeCenter(messages.get(i), top + i);
			}catch (IllegalArgumentException e) {
				System.out.println("Probleme");
				System.out.println("topi"+i+","+top);
			}
		}
		if (subscreen == null)
			messages.clear();
	}

	private void displayTiles(AsciiPanel terminal, int left, int top) {

		fov.update(player.x, player.y, player.z, player.visionRadius());
		
		for (int x = 0; x < screenWidth; x++){
			for (int y = 0; y < screenHeight; y++){
				int wx = x + left;
				int wy = y + top;

				if (player.canSee(wx, wy, player.z))
					terminal.write(world.glyph(wx, wy, player.z), x, y, world.color(wx, wy, player.z));
				else
					terminal.write(fov.tile(wx, wy, player.z).glyph(), x, y, Color.darkGray);
			}
		}
	}
	
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		int level = player.level();
		boolean visibility = player.hasVisibility();
		if (subscreen != null) {
			subscreen = subscreen.respondToUserInput(key);
		} else {
			switch (key.getKeyCode()){
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_Q: player.moveBy(-1, 0, 0); break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_D: player.moveBy( 1, 0, 0); break;
			case KeyEvent.VK_UP:
			case KeyEvent.VK_Z: player.moveBy( 0,-1, 0); break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_S: player.moveBy( 0, 1, 0); break;
			case KeyEvent.VK_A: player.moveBy(-1,-1, 0); break;
			case KeyEvent.VK_E: player.moveBy( 1,-1, 0); break;
			case KeyEvent.VK_W: player.moveBy(-1, 1, 0); break;
			case KeyEvent.VK_C: player.moveBy( 1, 1, 0); break;
			case KeyEvent.VK_N: subscreen = new DropScreen(player, fov); break;
			case KeyEvent.VK_R: subscreen = new EatScreen(player); break;
			case KeyEvent.VK_Y: subscreen = new EquipScreen(player); break;
			case KeyEvent.VK_F: subscreen = new ExamineScreen(player); break;
			case KeyEvent.VK_SEMICOLON: subscreen = new LookScreen(player, "Looking", 
					player.x - getScrollX(), 
					player.y - getScrollY()); break;
			case KeyEvent.VK_X: 
				if (player.weapon() == null || player.weapon().rangedAttackValue() == 0)
					player.notify("You don't have a ranged weapon equiped.");
				else
					subscreen = new FireWeaponScreen(player,
						player.x - getScrollX(), 
						player.y - getScrollY()); break;
			case KeyEvent.VK_T: subscreen = new QuaffScreen(player); break;
			case KeyEvent.VK_G: subscreen = new ReadScreen(player,
						player.x - getScrollX(), 
						player.y - getScrollY()); break;
			case KeyEvent.VK_M : player.dig(player.x, player.y, player.z); break;
			case KeyEvent.VK_O : subscreen = new InteractScreen(player, world); break;
			case KeyEvent.VK_SPACE : subscreen = new SaveScreen(this); break;
			case KeyEvent.VK_ESCAPE : return new StartScreen();
			}
			
			switch (key.getKeyChar()){
			case ',': player.pickup(); break;
			case '<': 
				if (userIsTryingToExit())
					subscreen = userExits();
				else
					player.moveBy( 0, 0, -1); break;
			case '>': player.moveBy( 0, 0, 1); break;
			case '?': subscreen = new HelpScreen(); break;
			}
		}
		if (subscreen == null)
			world.update();
		if (player.level() > level)
			subscreen = new LevelUpScreen(player, player.level() - level);
		
		if (visibility != player.hasVisibility()) 
			if (!visibility)
				fov.startVisibility(player.z);
			else 
				fov.endVisibility();
		if (player.hp() < 1)
			return new LoseScreen(player);
		return this;
	}

	private boolean userIsTryingToExit(){
		return player.z == 0 && world.tile(player.x, player.y, player.z) == Tile.STAIRS_UP;
	}
	
	private Screen userExits(){
		for (Item item : player.inventory().getItems())
			if (item != null && item.name().equals("exit key"))
				return new WinScreen();
		player.doAction("need the exit key");
		return null;
	}

}