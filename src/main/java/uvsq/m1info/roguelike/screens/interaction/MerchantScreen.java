package uvsq.m1info.roguelike.screens.interaction;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.screens.Screen;

public class MerchantScreen implements Screen {
	private Creature player;
	private Creature merchant;
	private String letters;
	public MerchantScreen(Creature player, Creature merchant) {
		this.player = player;
		this.merchant = merchant;
		this.letters = "abcdefghijklmnopqrstuvwxyz";
	}
	@Override
	public void displayOutput(AsciiPanel terminal) {
			List<String> lines = this.getList();
			
			//
			int y = 5;
			int x = 10;


			terminal.clear(' ', 0, 0, 80, 24);
			
			terminal.writeCenter(merchant.name() + " : Welcome to my shop ! ", y++);
			terminal.writeCenter("------------------------------", y++);
			
			for (String line : lines){
				terminal.write(line, x, y++);
			}
			String stats = String.format("o:%d    %3d/%3d hp    %d/%d mana   ", 
					player.money(), player.hp(), player.maxHp(), player.mana(), player.maxMana());
			terminal.write(stats, 1, 23);
			
			terminal.repaint();
		
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		char c = key.getKeyChar();
		if (letters.indexOf(c) > -1 && merchant.cptStock() > letters.indexOf(c)){
			merchant.sellItem(player, letters.indexOf(c));
			return null;
		} else if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
			return null;
		} else {
			return this;
		}
	}
	
	private List<String> getList() {
		ArrayList<String> lines = new ArrayList<String>();
		for(int i = 0; i < merchant.cptStock(); i++) {
			Item item = merchant.stock(i);
			int price = merchant.price(i);
			if (item == null) {
				continue;
			}
			String line = letters.charAt(i) + " - " + item.glyph() + " " + item.name()
						+ " " + price;
			
			lines.add(line);
		}
		return lines;
	}
}
