package uvsq.m1info.roguelike.screens.interaction;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.map.World;
import uvsq.m1info.roguelike.screens.Screen;

import asciiPanel.AsciiPanel;

public class InteractScreen implements Screen {
	private Creature player;
	private World world;
	HashMap<String, Creature> neighbors;
	private int neighborsNumber;
	List<String> neighborsLocation;
	public InteractScreen(Creature player, World world){
		this.player = player;
		this.world = world;
	}
	@Override
	public void displayOutput(AsciiPanel terminal) {
		neighbors = world.neighboringPNJ(player.x, player.y, player.z);
		neighborsNumber = neighbors.entrySet().size();
		neighborsLocation = new ArrayList<String>(neighbors.keySet());
		if (neighborsNumber == 0) {
			terminal.writeCenter("There is nobody around.", 22);
			return;
		}
		/*
		if (neighborsNumber == 1){
			System.out.println("ttseul0");
			return;
		}*/
		terminal.writeCenter("Where is the person you want to talk to ?", 22);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		if (neighborsNumber == 0)
				return null;
		/*if (neighborsNumber == 1) {
			System.out.println("ttseul");
			return chooseInteractionScreen(neighbors.get(neighborsLocation.get(0)));
		}*/
		switch(key.getKeyCode()){
			case KeyEvent.VK_UP : return interractWithCase("up");
			case KeyEvent.VK_DOWN : return interractWithCase("down");
			case KeyEvent.VK_LEFT : return interractWithCase("left");
			case KeyEvent.VK_RIGHT : return interractWithCase("right");
			case KeyEvent.VK_ESCAPE : return null;
			default : return this;
		}
	}
	private Screen interractWithCase(String location) {
		if (neighborsLocation.contains(location))
			return  chooseInteractionScreen(neighbors.get(location));
		else {
			player.doAction("can't see anyone here");
			return null;
		}
	}
	
	private Screen chooseInteractionScreen(Creature creature) {
		if(!creature.canInteractWith(player)) {
			player.doAction("can't talk with the angered "+creature.name());
			return null;
		}
		if (creature.isMerchant()) {
			return new MerchantScreen(player, creature);
		}
		if (creature.isTamed()) {
			player.doAction("pet the " + creature.name().split(" ")[2]);
			return null;
		}
		return null;
	}
}
