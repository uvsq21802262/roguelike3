package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class HelpScreen implements Screen {

	/* 
	 * @see uvsq.m1info.roguelike.screens.Screen#displayOutput(asciiPanel.AsciiPanel)
	 * This class displays the purpose of the game and the role of each key
	 */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.clear();
		terminal.writeCenter("roguelike help", 1);
		//terminal.write("Descend the Caves Of Slight Danger, find the lost Teddy Bear, and return to", 1, 3);
		terminal.write("Survive in the after war wasteland and defeat the monsters", 1, 3);
		terminal.write("Find the lengendary treasure and come back", 1, 4);
		
		int y = 6;
		terminal.write("[?] for help", 2, y++);
		terminal.write("[,] to pick up", 2, y++);
		terminal.write("[n] to drop", 2, y++);
		terminal.write("[r] to eat", 2, y++);
		terminal.write("[y] to wear or wield", 2, y++);
		terminal.write("[f] to examine your items", 2, y++);
		terminal.write("[;] to look around", 2, y++);
		terminal.write("[v] to throw an item", 2, y++);
		terminal.write("[t] to quaff a potion", 2, y++);
		terminal.write("[g] to read something", 2, y++);
		terminal.write("[o] to talk with a merchant", 2, y++);
		
		terminal.writeCenter("-- press any key to continue --", 22);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return null;
	}
}
