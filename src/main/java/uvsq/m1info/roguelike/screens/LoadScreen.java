package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;

import uvsq.m1info.roguelike.Persistance;

import asciiPanel.AsciiPanel;

public class LoadScreen implements Screen {
	
	/**
	 * This class allows to load the game
	 * Called by PlayScreen
	 */

	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("-- press [space] to resume --", 21);
		terminal.writeCenter("-- press [enter] to restart --", 22);		
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		PlayScreen p = null;
		if (key.getKeyCode() == KeyEvent.VK_ENTER) {
			return new StartScreen();
		}else if (key.getKeyCode() == KeyEvent.VK_SPACE)
			try {
				p = Persistance.resume();
				return p;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return this;
	}

}
