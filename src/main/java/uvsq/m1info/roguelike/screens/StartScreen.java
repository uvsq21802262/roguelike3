package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

/**
 * @author LY_Famille
 *this class displays the homepage in the game
 */
public class StartScreen implements Screen {

	
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("Le meilleur jeu du monde", 5);
		terminal.writeCenter("-- press [enter] to start --", 20);
		terminal.writeCenter("-- press [space] to load a game --", 21);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_ENTER : return new PlayScreen();
			case KeyEvent.VK_SPACE : return new LoadScreen();
			default : return this;
		}
	}
}

