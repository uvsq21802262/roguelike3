package uvsq.m1info.roguelike.screens.inventory;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.screens.Screen;

public class ReadScreen extends InventoryBasedScreen {

	private int sx;
	private int sy;
	
	public ReadScreen(Creature player, int sx, int sy) {
		super(player);
		this.sx = sx;
		this.sy = sy;
	}

	@Override
	protected String getVerb() {
		return "read";
	}

	@Override
	protected boolean isAcceptable(Item item) {
		return !item.writtenSpells().isEmpty();
	}

	@Override
	protected Screen use(Item item) {
		return new ReadSpellScreen(player, sx, sy, item);
	}

}
