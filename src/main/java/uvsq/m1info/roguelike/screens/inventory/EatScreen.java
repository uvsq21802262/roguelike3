package uvsq.m1info.roguelike.screens.inventory;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.screens.Screen;

public class EatScreen extends InventoryBasedScreen {

	public EatScreen(Creature player) {
		super(player);
	}

	@Override
	protected String getVerb() {
		return "eat";
	}

	@Override
	protected boolean isAcceptable(Item item) {
		return item.foodValue() != 0;
	}

	@Override
	protected Screen use(Item item) {
		player.eat(item);
		return null;
	}
}
