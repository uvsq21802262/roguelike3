package uvsq.m1info.roguelike.screens.inventory;

import uvsq.m1info.roguelike.FieldOfView;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.screens.Screen;

public class DropScreen extends InventoryBasedScreen {
	private FieldOfView fov;
	public DropScreen(Creature player, FieldOfView fov) {
		super(player);
		this.fov = fov;
	}

	@Override
	protected String getVerb() { 
		return "drop"; 
	}

	@Override
	protected boolean isAcceptable(Item item) { 
		return true; 
	}
	
	@Override
	protected Screen use(Item item) { 
		Item toDrop = player.drop(item); 
		if (toDrop != null){
			if (toDrop.isTorch()){
				fov.update(item.getX(), item.getY(), item.getZ(), 3);
			}
		}
		return null;
	}
}
