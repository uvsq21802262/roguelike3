package uvsq.m1info.roguelike.screens.inventory;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.screens.Screen;

public class EquipScreen extends InventoryBasedScreen {

	public EquipScreen(Creature player) {
		super(player);
	}

	protected String getVerb() {
		return "wear or wield";
	}

	protected boolean isAcceptable(Item item) {
		return item.isArmor() || item.isWeapon() || item.isPickaxe() || item.isShovel();
	}

	protected Screen use(Item item) {
		player.equip(item);
		return null;
	}
}
