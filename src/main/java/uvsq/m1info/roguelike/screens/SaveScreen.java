package uvsq.m1info.roguelike.screens;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.Serializable;

import uvsq.m1info.roguelike.Persistance;

import asciiPanel.AsciiPanel;

public class SaveScreen implements Screen, Serializable {

	/**
	 * This class allows to save the game 
	 */
	private static final long serialVersionUID = -2799721125018978228L;
	private PlayScreen playScreen;
	SaveScreen(PlayScreen playScreen) {
		this.playScreen = playScreen;
	}
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.clear(' ', 0, 0, 80, 23);
		terminal.writeCenter("-- press [space] to save --", 21);
		terminal.writeCenter("-- press [escape] to resume --", 22); 
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
			return null;
		}else if (key.getKeyCode() == KeyEvent.VK_SPACE) {
			try {
				Persistance.save(playScreen);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
