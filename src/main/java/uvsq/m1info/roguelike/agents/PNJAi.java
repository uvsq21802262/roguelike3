package uvsq.m1info.roguelike.agents;


public class PNJAi extends CreatureAi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6273350748869895425L;
	protected Creature target;
	public Creature target() { return target; }
	public PNJAi(Creature creature) {
		super(creature);
	}
	public void attackedBy(Creature creature) {
		target = creature;
	}
	
	public void onUpdate(){
		if (target != null && creature.canSee(target.x, target.y, target.z))
			hunt(target);
		else target = null;
	}
	
	
}
