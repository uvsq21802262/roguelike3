package uvsq.m1info.roguelike.agents;


import uvsq.m1info.roguelike.StuffFactory;

public class FungusAi extends CreatureAi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3253280457838236114L;
	private StuffFactory factory;
	private int spreadcount;
	
	public FungusAi(Creature creature, StuffFactory factory) {
		super(creature);
		this.factory = factory;
	}

	public void onUpdate(){
		if (spreadcount < 5 && Math.random() < 0.01)
			spread();
	}
	
	private void spread(){
		int x = creature.x + (int)(Math.random() * 11) - 5;
		int y = creature.y + (int)(Math.random() * 11) - 5;
		
		if (!creature.canEnter(x, y, creature.z))
			return;
		
		creature.doAction("spawn a child");
		Creature child = factory.newCreature("Fungus", creature.z);
		child.setLocation(x, y, creature.z);
		spreadcount++;
	}
}
