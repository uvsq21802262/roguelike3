package uvsq.m1info.roguelike.agents;

import java.io.Serializable;
import java.util.List;


import uvsq.m1info.roguelike.LevelUpController;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Line;
import uvsq.m1info.roguelike.map.Path;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;

public class CreatureAi implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2044591488230660781L;
	protected Creature creature;
	protected Creature attacker = null;
	public CreatureAi(Creature creature){
		this.creature = creature;
		this.creature.setCreatureAi(this);
	}
	
	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			creature.setLocation(x, y, z);
		} else {
			creature.doAction("bump into an obstacle");
		}
	}
	
	public void onUpdate(){ }
	
	public void onNotify(String message){ }

	public boolean canSee(int wx, int wy, int wz) {
		if (creature.z != wz)
			return false;
		int deltaX = creature.x - wx;
		int deltaY = creature.y - wy;
		if (deltaX * deltaX + deltaY * deltaY > creature.visionRadius() * creature.visionRadius () )
			return false;
		for (Point p : new Line(creature.x, creature.y, wx, wy)){
			if (creature.realTile(p.x, p.y, wz).isGround() || p.x == wx && p.y == wy)
				continue;
			return false;
		}
		return true;
	}
	
	public void wander(){
    	List<Point> n = Point.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.x + neighbor.x, creature.y + neighbor.y, creature.y);
    		if(other == null || other.name() != creature.name()) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
	}

	public void onGainLevel() {
		new LevelUpController().autoLevelUp(creature);
	}

	public Tile rememberedTile(int wx, int wy, int wz) {
		return Tile.UNKNOWN;
	}

	protected boolean canThrowAt(Creature other) {
		return creature.canSee(other.x, other.y, other.z)
			&& getWeaponToThrow() != null;
	}

	protected Item getWeaponToThrow() {
		Item toThrow = null;
		
		for (Item item : creature.inventory().getItems()){
			if (item == null || creature.weapon() == item || creature.armor() == item)
				continue;
			
			if (toThrow == null || item.thrownAttackValue() > toThrow.attackValue())
				toThrow = item;
		}
		
		return toThrow;
	}

	protected boolean canRangedWeaponAttack(Creature other) {
		return creature.weapon() != null
		    && creature.weapon().rangedAttackValue() > 0
		    && creature.canSee(other.x, other.y, other.z);
	}

	protected boolean canPickup() {
		return creature.item(creature.x, creature.y, creature.z) != null
			&& !creature.inventory().isFull();
	}

	public void hunt(Creature target) {
		List<Point> points = new Path(creature, target.x, target.y).points();
		if (points == null || points.size() == 0) {
			creature.moveBy(target.x - creature.x, target.y - creature.y, 0);
			return;
		}
		int mx = points.get(0).x - creature.x;
		int my = points.get(0).y - creature.y;
		creature.moveBy(mx, my, 0);
	}
	
	public Creature attacker() {
		return attacker;
	}
	
	public void flee(Creature target) {
		int mx = target.x - creature.x;
		if (mx != 0) 
			mx = (target.x - creature.x) / Math.abs(target.x - creature.x);
		int my = target.y - creature.y;
		if (my != 0)
			my = (target.y - creature.y) / Math.abs(target.y - creature.y);
		mx = -mx;
		my = -my;
		//if(creature.canEnter(creature.x + mx, creature.y + my, creature.z)) {
			creature.moveBy(mx, my, 0);
		/*
		 * 	return;
		}
		if (mx == 0) {
			mx = (int)(Math.random()*2) ;
			for (int i = 0; i < 2; i++) {
				int x = creature.x + (mx % 2) - 1;
				int y = creature.y + my;
				if(creature.canEnter(x, y, creature.z)) {
					creature.moveBy(mx, my, 0);
					return;
				}
				mx++;
			}
		}
		if (my == 0) {
			my = (int)(Math.random()*2) - 1;
			for (int i = 0; i < 2; i++) {
				if(creature.canEnter(creature.x + mx, creature.y + ( my % 2 ) - 1, creature.z)) {
					creature.moveBy(mx, my, 0);
					return;
				}
				my++;
			}
		}
	*/
	}

	protected boolean canUseBetterEquipment() {
		int currentWeaponRating = creature.weapon() == null ? 0 : creature.weapon().attackValue() + creature.weapon().rangedAttackValue();
		int currentArmorRating = creature.armor() == null ? 0 : creature.armor().defenseValue();
		
		for (Item item : creature.inventory().getItems()){
			if (item == null)
				continue;
			
			if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
					|| item.isArmor() && item.defenseValue() > currentArmorRating)
				return true;
		}
		
		return false;
	}

	protected void useBetterEquipment() {

		System.out.print("coosebetta");
		int currentWeaponRating = creature.weapon() == null ? 0 : creature.weapon().attackValue() + creature.weapon().rangedAttackValue();
		int currentArmorRating = creature.armor() == null ? 0 : creature.armor().defenseValue();
		
		for (Item item : creature.inventory().getItems()){
			if (item == null)
				continue;
			
			boolean isArmor = item.attackValue() + item.rangedAttackValue() < item.defenseValue();
			
			if (item.attackValue() + item.rangedAttackValue() > currentWeaponRating
					|| isArmor && item.defenseValue() > currentArmorRating) {
				creature.equip(item);
			}
		}
	}

	public void attackedBy(Creature creature) {
		attacker = creature;
	}

	public void sellItem(Creature creature, int itemIndex) { }

	public Creature target(){ 
		return null; 
	}

	public void tamedBy(Creature creature) { }

	public Creature master() { return null; }
}


