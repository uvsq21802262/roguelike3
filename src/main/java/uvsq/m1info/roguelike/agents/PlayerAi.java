package uvsq.m1info.roguelike.agents;

import java.util.List;


import uvsq.m1info.roguelike.FieldOfView;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Tile;

public class PlayerAi extends CreatureAi {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8759727247502213167L;
	private List<String> messages;
	private FieldOfView fov;
	
	public PlayerAi(Creature creature, List<String> messages, FieldOfView fov) {
		super(creature);
		this.messages = messages;
		this.fov = fov;
	}
	
	public void onEnter(int x, int y, int z, Tile tile){
		if (tile.isGround()){
			creature.x = x;
			creature.y = y;
			creature.z = z;
			Item item = creature.item(creature.x, creature.y, creature.z);
			if (item != null)
				creature.notify("There's a " + item.name() + " here.");
		} else if (tile.isWall()) {
			creature.mine(x, y, z);
		}else if (tile.isChest()){
			creature.openChest(x, y, z);
		}
	}
	
	public void onNotify(String message){
		messages.add(message);
	}
	
	public boolean canSee(int wx, int wy, int wz) {
		return fov.isVisible(wx, wy, wz);
	}
	
	public void onGainLevel(){
	}

	public Tile rememberedTile(int wx, int wy, int wz) {
		return fov.tile(wx, wy, wz);
	}
}
