package uvsq.m1info.roguelike.agents;

import java.util.List;

import uvsq.m1info.roguelike.map.Path;
import uvsq.m1info.roguelike.map.Point;


public class CannibalisticZombieAi extends ZombieAi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Creature player;
	public Creature getPlayer() { 
		return player;
	}
	
	public CannibalisticZombieAi(Creature creature, Creature target) {
		super(creature, target);
		this.player = target;
	}
	
	public void onUpdate(){
		if (Math.random() < 0.2)
			return;
		updateTarget();
		if (canSee(player.x, player.y, player.z)) {
			hunt(player);
			return;
		}
		if (target == null) {
			target = lookForNewTargets();
		}
		if (target != null && target.isAlive() && canSee(target.x, target.y, target.z)) {
			hunt(target);
		}
		else {
			wander();
			target = null;
		}
	}
	
	private Creature lookForNewTargets() {
		List<Point> neighbors = Point.neighborsN(creature.visionRadius());
		int shortestDistance = Integer.MAX_VALUE;
		Creature newTarget = null;
		for (Point p : neighbors) {
			int targetX = creature.x + p.x;
			int targetY = creature.y + p.y;
			Creature temp = creature.creature(targetX, targetY, creature.z);
			if (temp == null || !temp.isZombie() || temp.isCannibalisticZombie() || !temp.isAlive())
				continue;
			List<Point> points = new Path(creature, targetX, targetY).points();
			if (points == null || points.size() == 0) {
				return temp;
			}
			if (points.size() < shortestDistance) {
				shortestDistance = points.size();
				newTarget = temp;
			}
		}
		return newTarget;
	}

	@Override
	public void wander(){
    	List<Point> n = Point.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.x + neighbor.x, creature.y + neighbor.y, creature.y);
    		if(other == null) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
	}
	
	private void updateTarget() {
		if ( target == null ){
			//System.out.println("nullnewTargett :" );
			target = newTarget();
			return;
		}
		if (!creature.canSee(target.x, target.y, target.z)) {
			//System.out.println("can't see wut");
			target = null;
		}
	}
	
	private Creature newTarget() {
		List<Point> fov = Point.neighborsN(this.creature.visionRadius());
		for (Point p : fov) { 
			Creature creature = this.creature.creature(this.creature.x + p.x, this.creature.y + p.y, this.creature.z);
			if (creature != null && creature != this.creature) {
				return creature;
			}
		}
		return null;
	}
	
}
