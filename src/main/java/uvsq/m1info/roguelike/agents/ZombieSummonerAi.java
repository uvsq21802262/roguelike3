package uvsq.m1info.roguelike.agents;

import uvsq.m1info.roguelike.StuffFactory;

public class ZombieSummonerAi extends CreatureAi {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6342259167818941911L;

	private Creature player;
	public Creature getPlayer() { return player;}
	private int cpt;
	
	private StuffFactory factory;
	public StuffFactory getStuffFactory() { return factory;}
	
	
	public ZombieSummonerAi(Creature creature, Creature player, StuffFactory factory) {
		super(creature);
		this.player = player;
		this.factory = factory;
		this.cpt = 0;
	}

	public void onUpdate(){
		cpt ++;
		if (cpt%10 == 0 && player.canSee(creature.x, creature.y, creature.z))
			summon();
		if (Math.random() < 0.2)
			return;
		if (creature.canSee(player.x, player.y, player.z)) {
			flee(player);
		}
		else
			wander();
	}
	
	protected void summon(){
		if(factory==null) 
			return;
		int x = creature.x + (int)(Math.random() * 5) - 3;
		int y = creature.y + (int)(Math.random() * 5) - 3;
		
		if (!creature.canEnter(x, y, creature.z))
			return;

		creature.doAction("spawn a child");
		Creature child = factory.newAggressiveCreature("LesserZombie", creature.z, player);
		child.setLocation(x, y, creature.z);
	}
}
