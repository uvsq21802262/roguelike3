package uvsq.m1info.roguelike.agents;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;

import uvsq.m1info.roguelike.Effect;
import uvsq.m1info.roguelike.Spell;
import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.items.Inventory;
import uvsq.m1info.roguelike.items.Item;
import uvsq.m1info.roguelike.map.Line;
import uvsq.m1info.roguelike.map.Location;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;
import uvsq.m1info.roguelike.map.World;

public class Creature implements Location, Serializable {
	private static final long serialVersionUID = -2319394183562360953L;
	private World world;
	public int x;
	public int y;
	public int z;

	private char glyph;
	private String name;
	private Color color;
	private CreatureAi ai;
	private int money;
	private int additionalLives;
	private int maxHp;
	private int hp;
	private int maxMana;
	private int mana;
	private int maxFood;
	private int food;
	private int attackValue;
	private int defenseValue;
	private int miningValue;

	private String causeOfDeath;
	private int visionRadius;
	private Inventory inventory;
	private Item[] equipment;
	private int[] prices;

	private int regenHpCooldown;
	private int regenHpPer1000;
	private int regenManaCooldown;
	private int regenManaPer1000;

	private int xp;
	private int level;
	
	private List<Effect> effects;
	
	
	
	public void modifyMaxHp(int amount, String causeOfDeath) {
		maxHp += amount;
		if (hp > maxHp) {
			modifyHp(maxHp - hp, causeOfDeath);
		}
	}

	public void addLifes(int value) {
		additionalLives += value;
		if (value < 0)
			doAction("are drained out of energy");
		if (value > 0)
			doAction("are fueled with energy");
	}

	private void modifyMoney(int amount) {
		if (amount > 0) {
			double coinMultiplicator = 1;
			for (int i = 0; i < equipment.length; i++) {
				coinMultiplicator *= equipment[i] != null ? equipment[i].coinMultiplicator() : 1;
			}
			amount = (int) (coinMultiplicator * amount);
			doAction("gain " + amount + " coins");
		}
		money += amount;
	}
	
	private void equipmentDecay(Item equipment) {
		if (equipment != null) {
			equipment.decreaseDurationValue();
			if (equipment.isBroken() && isPlayer()) {
				notify("Your " + equipment.name() + " broke");
				getRidOf(equipment);
			}
		}
	}

	public int miningValue() {
		return miningValue + ((pickaxe() != null && pickaxe().durationValue() > 0) ? pickaxe().miningValue() : 0);
	};

	public int attackValue() {
		return attackValue + (weapon() == null ? 0 : weapon().attackValue())
				+ (armor() == null ? 0 : armor().attackValue());
	}

	public int defenseValue() {
		return defenseValue + (weapon() == null ? 0 : weapon().defenseValue())
				+ (armor() == null ? 0 : armor().defenseValue());
	}
	public int xp() {
		return xp;
	}
	
	public void noXp() {
		xp = -1;
	}

	public boolean givesXp() {
		return xp != -1;
	}
	public void modifyXp(int amount) {
		xp += amount;

		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);

		while (xp > (int) (Math.pow(level, 1.75) * 25)) {
			level++;
			doAction("advance to level %d", level);
			ai.onGainLevel();
			modifyHp(level * 2, "Died from having a negative level?");
		}
	}

	public int level() {
		return level;
	}
	public void modifyRegenHpPer1000(int amount) {
		regenHpPer1000 += amount;
	}

	public List<Effect> effects() {
		return effects;
	}

	public int maxMana() {
		return maxMana;
	}

	public void modifyMaxMana(int amount) {
		maxMana += amount;
	}

	public int mana() {
		return mana;
	}

	public void modifyMana(int amount) {
		mana = Math.max(0, Math.min(mana + amount, maxMana));
	}


	public void modifyRegenManaPer1000(int amount) {
		regenManaPer1000 += amount;
	}

	public String causeOfDeath() {
		return causeOfDeath;
	}
	
	private int cptStock;
	

	public int cptStock() {
		return cptStock;
	}

	public void setStockRoom(int n) {
		this.inventory = new Inventory(n);
		this.prices = new int[n];
	}

	public Item stock(int i) {
		if (i >= cptStock)
			return null;
		return inventory.getItems()[i];
	}

	public int price(int i) {
		if (i >= cptStock)
			return -1;
		return prices[i];
	}

	public void addItemToStock(Item item, int price) {
		if (cptStock == inventory.getItems().length)
			return;
		item.setLocation(this);
		prices[cptStock] = price;
		inventory.add(item);
		cptStock++;
	}

	public boolean canInteractWith(Creature creature) {
		return ai.target() != creature;
	}
	
	public Item key(){
		return inventory.get("key");
	}

	public Creature(World world, char glyph, Color color, String name, int maxHp, int attack, int defense) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
		this.visionRadius = 9;
		this.name = name;
		this.inventory = new Inventory(20);
		this.maxFood = 1000;
		this.food = maxFood / 3 * 2;
		this.level = 1;
		this.regenHpPer1000 = 10;
		this.effects = new ArrayList<Effect>();
		this.maxMana = 100;
		this.mana = maxMana;
		this.regenManaPer1000 = 20;
		this.visibility = false;
		this.miningValue = 1;
		this.equipment = new Item[4];
		this.money = 0;
		this.cptStock = 0;
		this.xp = 0;
	}
	private boolean slipping = false;
	private int cptMove = 0;

	private int breathCpt = 0;
	
	public void moveBy(int mx, int my, int mz) {

		Tile actualTile = world.tile(x, y, z);
		if (breathCpt == 10){
			modifyHp(-maxHp, "Death by suffocation.");
			return;
		}
		if (!canFly()) {
			if(isPlayer()) 
				if (actualTile == Tile.WATER) {
					cptMove = (cptMove + 1) % 2;
					breathCpt++;
					if (cptMove == 1)
						return;
					color = AsciiPanel.blue;
				}else {
					breathCpt = 0;
					color = AsciiPanel.brightWhite;
					if (actualTile == Tile.POISON) 
						this.addEffect(Effect.POISON);
				}
			if (actualTile == Tile.SWAMP) 
				if (isPlayer())
					if (Math.random() < 0.7) {
						doAction("get trapped");
						return;
					}
				else
					if(Math.random() < 0.2) {
						doAction("get trapped");
						return;
					}
		}
		
		if (mx == 0 && my == 0 && mz == 0)
			return;

		Tile tile = world.tile(x + mx, y + my, z + mz);

		if (mz == -1) {
			if (tile == Tile.STAIRS_DOWN) 
				doAction("walk up the stairs to level %d", z + mz + 1);
			else {
				doAction("try to go up but are stopped by the cave ceiling");
				return;
			}
		} else if (mz == 1) {
			if (tile == Tile.STAIRS_UP) {
				doAction("walk down the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go down but are stopped by the cave floor");
				return;
			}
		}

		Creature other = world.creature(x + mx, y + my, z + mz);

		modifyFood(-1);
		if (other == null) {
			ai.onEnter(x + mx, y + my, z + mz, tile);
			if (!canFly() && (tile == Tile.ICE || (tile == Tile.FLOOR && world.tile(x, y, z) == Tile.ICE))) {
				if (!slipping) {
					doAction("slip");
					slipping = true;
				}
				moveBy(mx, my, mz);
			}
			slipping = false;
		} else
			meleeAttack(other);
	}

	public void meleeAttack(Creature other) {
		commonAttack(other, attackValue(), "attack the %s for %d damage", other.name);
	}

	private void throwAttack(Item item, Creature other) {
		commonAttack(other, attackValue / 2 + item.thrownAttackValue(), "throw a %s at the %s for %d damage",
				item.name(), other.name);
		other.addEffect(item.quaffEffect(), item);
	}

	public void rangedWeaponAttack(Creature other) {
		commonAttack(other, attackValue / 2 + weapon().rangedAttackValue(), "fire a %s at the %s for %d damage",
				weapon().name(), other.name);
	}

	private void commonAttack(Creature other, int attack, String action, Object... params) {
		modifyFood(-2);

		int amount = Math.max(0, attack - other.defenseValue());
		amount = (int) (Math.random() * amount) + 1;
		Object[] params2 = new Object[params.length + 1];
		
		for (int i = 0; i < params.length; i++)
			params2[i] = params[i];
		params2[params2.length - 1] = amount;

		doAction(action, params2);
		other.notifyAttack(this);
		equipmentDecay(weapon());
		equipmentDecay(other.armor());
		
		other.modifyHp(-amount, "Killed by a " + name);
		if (other.hp < 1) {
			if (isCannibalisticZombie() && other.isZombie() && other.givesXp() == true) {
				attackValue = Math.min(attackValue + other.attackValue() / 2 + attackValue(), 200);
				defenseValue = Math.min(defenseValue + other.defenseValue() / 2 + defenseValue(), 100);
				maxHp = Math.min(maxHp + other.maxHp(), 200);
				modifyHp(maxHp(), "Killed by healing");
			}
			if(other.givesXp() == true) {
				gainXp(other);
				gainMoney(other);
			}
		}
	}
	public Creature attacker() {
		return ai.attacker();
	}
	
	private void notifyAttack(Creature creature) {
		ai.attackedBy(creature);
	}

	private void gainMoney(Creature other) {
		if (isTamed()) 
			master().modifyMoney(other.money);
		else
			modifyMoney(other.money);
	}

	public void gainXp(Creature other) {
		if (!givesXp())
			return;
		int amount = other.maxHp + other.attackValue() + other.defenseValue() - level;

		if (amount > 0)
			if (isTamed()) {
				master().modifyXp(amount / 2);
				modifyXp(amount);
			}
			else
				modifyXp(amount);
	}

	public void modifyHp(int amount, String causeOfDeath) {
		hp += amount;
		this.causeOfDeath = causeOfDeath;
		if (hp > maxHp) {
			hp = maxHp;
		} else if (hp < 1) {
			if (isTameable() && attacker()!= null && attacker().isPlayer()) {
				hp = maxHp;
				attacker().doAction("tame the animal");
				ai.tamedBy(attacker());
				name = "player's pet " + name.split(" ")[1];
			}else if (additionalLives > 0) {
				additionalLives--;
				maxHp = maxHp / 2;
				if (maxHp <= 0)
					maxHp = 1;
				hp = maxHp;
				doAction("get back up");
			} else {
				doAction("die");
				leaveCorpse();
				dies();
				world.remove(this);
			}
		}
	}
	
	private void leaveCorpse() {
		Item corpse = new Item('%', color, name + " corpse");
		corpse.modifyFoodValue(maxHp * 5);
		corpse.setDurationValue(10);
		world.addAtEmptySpace(corpse, x, y, z);
		for (Item item : inventory.getItems()) {
			if (item != null)
				drop(item);
		}
	}

	public void openChest(int wx, int wy, int wz) {
		Item key = key();
		if (key != null) {
			doAction("open the chest");
			int amount = (int)(Math.random() * 100 * (z + 1));
			modifyMoney(amount);
			world.openChest(wx, wy, wz, this);
			getRidOf(key);
		}else
			doAction("need a key");
	}
	
	public void mine(int wx, int wy, int wz) {
		modifyFood(-10);
		world.mine(wx, wy, wz, miningValue());
		doAction("mine");
		equipmentDecay(pickaxe());
	}

	public void dig(int wx, int wy, int wz) {
		modifyFood(-50);
		if (shovel() == null) {
			doAction("don't have a shovel");
			return;
		}
		if (!world.tile(x, y, z).isDiggable()) {
			doAction("can't dig here");
			return;
		}
		if (z == world.depth() - 1) {
			doAction("can't go deeper");
			return;
		}
		if (world.tile(x, y, z + 1).isWall() || world.tile(x, y, z + 1) == Tile.STAIRS_DOWN
				|| world.creature(x, y, z + 1) != null || world.item(x, y, z + 1) != null) {
			notify("Something is blocking the way");
			return;
		}
		doAction("dig");
		equipmentDecay(shovel());
		world.dig(wx, wy, wz);
	}

	public void sellItem(Creature creature, int itemIndex) {
		ai.sellItem(creature, itemIndex);
	}

	public void update() {
		if (!isAlive())
			return;
		modifyFood(-1);
		regenerateHealth();
		regenerateMana();
		updateEffects();
		ai.onUpdate();
	}

	private void updateEffects() {
		List<Effect> done = new ArrayList<Effect>();
		for (Effect effect : effects) {
			effect.update(this);
			if (effect.isDone()) {
				effect.end(this);
				done.add(effect);
			}
		}
		effects.removeAll(done);
	}

	private void regenerateHealth() {
		regenHpCooldown -= regenHpPer1000;
		if (regenHpCooldown < 0) {
			if (hp < maxHp) {
				modifyHp(1, "Died from regenerating health?");
				modifyFood(-1);
			}
			regenHpCooldown += 1000;
		}
	}

	private void regenerateMana() {
		regenManaCooldown -= regenManaPer1000;
		if (regenManaCooldown < 0) {
			if (mana < maxMana) {
				modifyMana(1);
				modifyFood(-1);
			}
			regenManaCooldown += 1000;
		}
	}

	public boolean canEnter(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
	}

	public void notify(String message, Object... params) {
		ai.onNotify(String.format(message, params));
	}

	public void doAction(String message, Object... params) {
		for (Creature other : getCreaturesWhoSeeMe()) {
			if (other == this) {
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
		}
	}

	public void doAction(Item item, String message, Object... params) {
		if (hp < 1)
			return;

		for (Creature other : getCreaturesWhoSeeMe()) {
			if (other == this) {
				other.notify("You " + message + ".", params);
			} else {
				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
			}
		}
	}

	private List<Creature> getCreaturesWhoSeeMe() {
		List<Creature> others = new ArrayList<Creature>();
		int r = 9;
		for (int ox = -r; ox < r + 1; ox++) {
			for (int oy = -r; oy < r + 1; oy++) {
				if (ox * ox + oy * oy > r * r)
					continue;
				Creature other = world.creature(x + ox, y + oy, z);
				if (other == null)
					continue;

				others.add(other);
			}
		}
		return others;
	}

	private String makeSecondPerson(String text) {
		String[] words = text.split(" ");
		words[0] = words[0] + "s";

		StringBuilder builder = new StringBuilder();
		for (String word : words) {
			builder.append(" ");
			builder.append(word);
		}

		return builder.toString().trim();
	}

	public boolean canSee(int wx, int wy, int wz) {
		return (detectCreatures > 0 && world.creature(wx, wy, wz) != null || ai.canSee(wx, wy, wz));
	}
	private boolean isAlive = true;
	private void dies() {
		isAlive = false;
	}
	public boolean isAlive(){
		return isAlive;
	}
	
	public Tile realTile(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz);
	}

	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.tile(wx, wy, wz);
		else
			return ai.rememberedTile(wx, wy, wz);
	}

	public Creature creature(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.creature(wx, wy, wz);
		else
			return null;
	}

	public void pickup() {
		Item item = world.item(x, y, z);
		
		if (inventory.isFull() || item == null) {
			doAction("grab at the ground");
		} else {
			doAction("pickup a %s", item.name());
			world.remove(x, y, z);
			inventory.add(item);
			item.setLocation(this);
		}
	}

	public Item drop(Item item) {
		if (world.addAtEmptySpace(item, x, y, z)) {
			doAction("drop a " + item.name());
			inventory.remove(item);
			unequip(item);
			return item;
		} else {
			notify("There's nowhere to drop the %s.", item.name());
			return null;
		}
	}

	public void modifyFood(int amount) {
		food += amount;

		if (food > maxFood) {
			maxFood = (maxFood + food) / 2;
			food = maxFood;
			notify("You can't belive your stomach can hold that much!");
			modifyHp(-1, "Killed by overeating.");
		} else if (food < 1 && isPlayer()) {
			modifyHp(maxHp, "Starved to death.");
		}
	}

	public boolean isPlayer() {
		return glyph == '@';
	}

	public boolean isMerchant() {
		return name.contains("merchant");
	}

	public boolean isZombie() {
		return name.contains("zombie");
	}
	
	public boolean isCannibalisticZombie() {
		return isZombie() && name.contains("cannibalistic");
	}
	
	public boolean isPNJ() {
		return isMerchant() || isTamed();
	}

	public boolean canFly() {
		return name.contains("bat");
	}
	
	public boolean isTameable() {
		return name.contains("wild");
	}
	
	public boolean isTamed() {
		return ai.master() != null;
	}
	
	private Creature master(){
		return ai.master();
	}
	
	public void eat(Item item) {
		doAction("eat a " + item.name());
		consume(item);
	}

	public void quaff(Item item) {
		doAction("quaff a " + item.name());
		consume(item);
	}

	private void consume(Item item) {
		if (item.foodValue() < 0)
			notify("Gross!");

		addEffect(item.quaffEffect(), item);
		modifyFood(item.foodValue());
		getRidOf(item);
	}

	private void addEffect(Effect effect, Item item) {
		if (effect == null)
			return;
		if (effects.contains(effect))
			notify("Nothing happened");
		effect.start(this, item);
		effects.add(effect);
	}
	
	private void addEffect(Effect effect) {
		if (effect == null)
			return;
		if (effects.contains(effect)) {
			notify("Nothing happened");
			return;
		}
		effect.start(this);
		effects.add(effect);
	}
	
	private void getRidOf(Item item) {
		inventory.remove(item);
		unequip(item);
	}

	private void putAt(Item item, int wx, int wy, int wz) {
		inventory.remove(item);
		unequip(item);
		world.addAtEmptySpace(item, wx, wy, wz);
	}

	public void unequip(Item item) {
		if (item == null)
			return;
		if (item == pickaxe()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setPickaxe(null);
		}
		if (item == shovel()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setShovel(null);
		}
		if (item == armor()) {
			if (hp > 0)
				doAction("remove a " + item.name());
			setArmor(null);
		} else if (item == weapon()) {
			if (hp > 0)
				doAction("put away a " + item.name());
			setWeapon(null);
		}
		applyMaxHpBonus(-item.maxHpBonus(), "Killed by " + item.name());
	}

	public void equip(Item item) {
		if (!inventory.contains(item)) {
			if (inventory.isFull()) {
				notify("Can't equip %s since you're holding too much stuff.", item.name());
				return;
			} else {
				world.remove(item);
				inventory.add(item);
			}
		}
		if (item.isPickaxe()) {
			unequip(pickaxe());
			doAction("carry a " + item.name());
			setPickaxe(item);
		}
		if (item.isShovel()) {
			unequip(shovel());
			doAction("carry a " + item.name());
			setShovel(item);
		}
		if (!item.isWeapon() && !item.isArmor())
			return;
		if (item.attackValue() + item.rangedAttackValue() >= item.defenseValue()) {
			unequip(weapon());
			doAction("wield a " + item.name());
			setWeapon(item);
		} else {
			unequip(armor());
			doAction("put on a " + item.name());
			setArmor(item);
		}
		applyMaxHpBonus(item.maxHpBonus(), "Killed by " + item.name());
		addLifes(item.additionalLives());
		item.setAdditionalLives(0);

	}

	private void applyMaxHpBonus(int value, String CauseOfDeath) {
		modifyMaxHp(value, CauseOfDeath);
		if (value > 0) {
			doAction("look healthier");
		}
		if (value < 0) {
			if (hp > 0)
				doAction("look less healthy");
		}
	}

	public Item item(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.item(wx, wy, wz);
		else
			return null;
	}

	public String details() {
		return String.format("  level:%d  attack:%d  defense:%d  hp:%d", level, attackValue(), defenseValue(), hp);
	}

	public void throwItem(Item item, int wx, int wy, int wz) {
		Point end = new Point(x, y, 0);

		for (Point p : new Line(x, y, wx, wy)) {
			if (!realTile(p.x, p.y, z).isGround())
				break;
			end = p;
		}

		wx = end.x;
		wy = end.y;

		Creature c = creature(wx, wy, wz);

		if (c != null)
			throwAttack(item, c);
		else
			doAction("throw a %s", item.name());

		if (item.quaffEffect() != null && c != null)
			getRidOf(item);
		else
			putAt(item, wx, wy, wz);
	}

	public void summonBat(int x, int y, int z) {
		Creature bat = StuffFactory.newSummonedBat();

		bat.setWorld(world);
		if (!bat.canEnter(x, y, z)) {
			return;
		}
		bat.setLocation(x, y, z);
		world.add(bat);
	}

	private int detectCreatures;

	public void modifyDetectCreatures(int amount) {
		detectCreatures += amount;
	}

	public void castSpell(Spell spell, int x2, int y2) {
		Creature other = creature(x2, y2, z);
		if (spell.manaCost() > mana) {
			doAction("point and mumble but nothing happens");
			return;
		} else if (other == null) {
			doAction("point and mumble at nothing");
			return;
		}
		other.addEffect(spell.effect());
		modifyMana(-spell.manaCost());
		System.out.println(mana);
	}

	public boolean hasVisibility() {
		return visibility;
	}

	public void addVisibility() {
		this.visibility = true;
	}

	public void removevisibility() {
		this.visibility = false;
	}

	private boolean visibility;

	@Override
	public Location location() {
		return this;
	}

	@Override
	public void setLocation(Location location) {
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
	}

	@Override
	public void setLocation(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getZ() {
		return z;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public void setWorld(World world) {
		this.world = world;
	}

	public char glyph() {
		return glyph;
	}

	
	public void setCreatureAi(CreatureAi ai) {
		this.ai = ai;
	}

	public Color color() {
		return color;
	}

	public int money() {
		return money;
	}
	public int maxHp() {
		return maxHp;
	}

	public int hp() {
		return hp;
	}
	
	public int additionalLives() {
		return additionalLives;
	}

	public void setMoney(int amount) {
		money += amount;
	}

	public void modifyAttackValue(int value) {
		attackValue += value;
	}
	public void modifyDefenseValue(int value) {
		defenseValue += value;
	}
	public void modifyVisionRadius(int value) {
		visionRadius += value;
	}

	public int visionRadius() {
		return visionRadius;
	}

	public String name() {
		return name;
	}

	public Inventory inventory() {
		return inventory;
	}

	public int maxFood() {
		return maxFood;
	}

	public int food() {
		return food;
	}

	public Item weapon() {
		return equipment[0];
	}

	public void setWeapon(Item item) {
		equipment[0] = item;
	}

	public Item armor() {
		return equipment[1];
	}

	public void setArmor(Item item) {
		equipment[1] = item;
	}

	public Item pickaxe() {
		return equipment[2];
	}

	public void setPickaxe(Item item) {
		equipment[2] = item;
	}

	public Item shovel() {
		return equipment[3];
	}

	public void setShovel(Item item) {
		equipment[3] = item;
	}

	
}