package uvsq.m1info.roguelike.agents;


public class StrongZombieAi extends ZombieAi {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -953251641401950336L;

	public StrongZombieAi(Creature creature, Creature target) {
		super(creature, target);
	}

	public void onUpdate(){
		if (creature.canSee(target.x, target.y, target.z)) {
			hunt(target);
		}
		else
			wander();
	}
}
