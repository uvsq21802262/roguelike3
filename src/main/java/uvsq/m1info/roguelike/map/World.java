package uvsq.m1info.roguelike.map;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uvsq.m1info.roguelike.StuffFactory;
import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.items.Item;

public class World implements Serializable {
	private static final long serialVersionUID = 3594814834473544557L;
	
	/**
	 * The tiles of the world created
	 */
	private Tile[][][] tiles;
	
	/**
	 * The i of the world created
	 */
	private Item[][][] items;
	
	/**
	 * The width of the world
	 */
	private int width;
	/**
	 * The height of the world
	 */

	private int height;
	/**
	 * The depth of the world
	 */
	private int depth;
	
	/**
	 * The creatures of the world
	 */
	private List<Creature> creatures;
	private List<Creature> creaturesTmp;
	
	/**
	 * Constructor not null
	 * @param tiles tiles used for building the world
	 */
	public World(Tile[][][] tiles){
		this.tiles = tiles;
		this.width = tiles.length;
		this.height = tiles[0].length;
		this.depth = tiles[0][0].length;
		this.creatures = new ArrayList<Creature>();
		this.items = new Item[width][height][depth];
	}

	/**
	 * Determine a creature at certain coordinate
	 * @param x coordinate x of the creature
	 * @param y coordinate y of the creature
	 * @param z coordinate z of the creature
	 * @return the creature at the coordinate
	 *         null if there isn't any creature
	 */
	public Creature creature(int x, int y, int z){
		for (Creature c : creatures){
			if (c.x == x && c.y == y && c.z == z)
				return c;
		}
		return null;
	}
	
	/**
	 * Determine a tile at certain coordinate
	 * @param x coordinate x of the tile
	 * @param y coordinate y of the tile
	 * @param z coordinate z of the tile
	 * @return the tile at the coordinate
	 */
	public Tile tile(int x, int y, int z){
		if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= depth)
			return Tile.BOUNDS;
		else
			return tiles[x][y][z];
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return the character used at certain coordinate
	 */
	public char glyph(int x, int y, int z){
		Creature creature = creature(x, y, z);
		if (creature != null)
			return creature.glyph();
		
		if (item(x,y,z) != null)
			return item(x,y,z).glyph();
		
		return tile(x, y, z).glyph();
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return the color at certain coordinate
	 */
	public Color color(int x, int y, int z){
		Creature creature = creature(x, y, z);
		if (creature != null)
			return creature.color();
		
		if (item(x,y,z) != null)
			return item(x,y,z).color();
		
		return tile(x, y, z).color();
	}

	/**
	 * Open the chest at certain coordinate
	 * @param x the coordinate x of a chest
	 * @param y the coordinate y of a chest
	 * @param z the coordinate z of a chest
	 * @param player the player
	 */
	public void openChest(int x, int y, int z, Creature player) {
		Tile tile = tile(x, y, z);
		if (!tile.isChest()){
			return;
		}else{
			tiles[x][y][z] = Tile.FLOOR;
			if (Math.random() < 0) {
				Creature mimic = StuffFactory.newMimic(x, y, z, player);
				mimic.setWorld(this);
				add(mimic);
			}else {
				if (Math.random() < 0.2)
					items[x][y][z] = StuffFactory.newItem("RandomSpellBook", x, y, z);
				else
					items[x][y][z] = StuffFactory.newFood();
			}
		}
	}
	
	/**
	 * Mine into a wall at certain coordinate
	 * @param x the coordinate x of a wall
	 * @param y the coordinate x of a wall
	 * @param z the coordinate x of a wall
	 * @param miningValue the ability of mining
	 */
	public void mine(int x, int y, int z, int miningValue) {
		Tile tile = tile(x, y, z);
		if (!tile.isWall()){
			return;
		}else{
			int res = tile.wallState() - miningValue;
			tiles[x][y][z] = Tile.wallState(res);
			if (tiles[x][y][z].isGround() && tile == Tile.WALLX){
				if (Math.random() < 0.8) 
					items[x][y][z] = StuffFactory.newItem("Food", x, y, z);
				else if (Math.random() < 0.5) 
					items[x][y][z] = StuffFactory.newItem("RandomRelic", x, y, z);
				else
					items[x][y][z] = StuffFactory.newItem("RandomCursedRelic", x, y, z);
			}
		}
	}
	
	
	/**
	 * Dig a stair at certain coordinate
	 * @param x coordinate x of a dig point
	 * @param y coordinate y of a dig point
	 * @param z coordinate z of a dig point
	 */
	public void dig(int x, int y, int z) {
		tiles[x][y][z] = Tile.STAIRS_DOWN;
		tiles[x][y][z + 1] = Tile.STAIRS_UP;
	}
	
	/**
	 * Add randomly a creature at empty space  
	 * @param creature creature needed to be added
	 * @param z level of the creature
	 */
	public void addAtEmptyLocation(Creature creature, int z){
		int x;
		int y;
		
		do {
			x = (int)(Math.random() * width);
			y = (int)(Math.random() * height);
		} 
		while (!tile(x,y,z).isGround() || creature(x,y,z) != null);
		
		creature.setLocation(x, y, z);
		creatures.add(creature);
	}
	
	/**
	 * Update creatures and items in the world
	 */
	public void update(){
		creaturesTmp = new ArrayList<Creature>();
		Creature player = null;
		List<Creature> toUpdate = new ArrayList<Creature>(creatures);
		for (Creature creature : toUpdate){
			if (creature.isPlayer()) 
				player = creature;
			creature.update();
		}
		if (player == null)
			return;
		for (int i = 0; i < width; i++) 
			for (int j = 0; j < height; j++) {
				Item item = items[i][j][player.z];
				if (item != null && item.isZombieCorpse()) {
					item.decreaseDurationValue();
					if (item.canRevive() && creature(i, j, player.z) == null) 
						if (creature(i, j, player.z) == null)
							revive(item, player);
						else
							item.resetDurationValue();
				}
			}
		creatures.addAll(creaturesTmp);
	}

	
	/**
	 * Revive a corpse
	 * @param corpse corpse needed to be revived
	 * @param player the player
	 */
	private void revive(Item corpse, Creature player) {
		Creature zombie = StuffFactory.newZombieFromCorpse(player, corpse);
		
		if (zombie == null) {
			return;
		}
		remove(corpse);
		zombie.noXp();
		zombie.setWorld(this);
		zombie.doAction("come back to life");
		creaturesTmp.add(zombie);
	}

	/**
	 * Remove certain creature from the wolrd
	 * @param other the creature needed to be removed
	 */
	public void remove(Creature other) {
		creatures.remove(other);
	}
	
	/**
	 * Remove certain item from the world
	 * @param item the item needed to be removed
	 */
	public void remove(Item item) {
		items[item.getX()][item.getY()][item.getZ()] = null;
	}
	
	/**
	 * @param x 
	 * @param y
	 * @param z
	 * @return item at certain coordinate
	 */
	public Item item(int x, int y, int z){
		return items[x][y][z];
	}
	
	
	/**
	 * Add randomly a item at empty space  
	 * @param item item needed to be added
	 * @param depth level of the item
	 */
	public void addAtEmptyLocation(Item item, int depth) {
		int x;
		int y;
		
		do {
			x = (int)(Math.random() * width);
			y = (int)(Math.random() * height);
		} while (!tile(x,y,depth).isGround() || item(x,y,depth) != null);
		item.setLocation(x, y, depth);
		items[x][y][depth] = item;
	}

	/**
	 * Remove item at a certain coordinates from the wolrd
	 * @param x the column
	 * @param y the line
	 * @param z the depth
	 */
	public void remove(int x, int y, int z) {
		items[x][y][z] = null;
	}

	/**
	 * Add a item at empty space with a specified coordinate 
	 * @param item item needed to be added
	 * @param x 
	 * @param y
	 * @param z
	 * @return true if added successfully
	 *         false otherwise
	 */
	public boolean addAtEmptySpace(Item item, int x, int y, int z){
		if (item == null)
			return true;
		
		List<Point> points = new ArrayList<Point>();
		List<Point> checked = new ArrayList<Point>();
		
		points.add(new Point(x, y, z));
		
		while (!points.isEmpty()){
			Point p = points.remove(0);
			checked.add(p);
			
			if (!tile(p.x, p.y, p.z).isGround())
				continue;
				
			if (items[p.x][p.y][p.z] == null){
				items[p.x][p.y][p.z] = item;
				item.setLocation(p.x, p.y, p.z);
				Creature c = this.creature(p.x, p.y, p.z);
				if (c != null)
					c.notify("A %s lands between your feet.", item.name());
				return true;
			} else {
				List<Point> neighbors = p.neighbors8();
				neighbors.removeAll(checked);
				points.addAll(neighbors);
			}
		}
		return false;
	}

	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return a map containing the pnj around (x,y,z)
	 */
	public HashMap<String, Creature> neighboringPNJ(int x, int y, int z) {
		HashMap<String, Creature> neighbors = new HashMap<String, Creature>();
		neighbors = addPNJtoNeighborhood(neighbors, "up", x, y - 1, z);
		neighbors = addPNJtoNeighborhood(neighbors, "right", x + 1, y, z);
		neighbors = addPNJtoNeighborhood(neighbors, "down", x, y + 1, z);
		neighbors = addPNJtoNeighborhood(neighbors, "left", x - 1, y, z);
		return neighbors;
	}
	
	/**
	 * Check if there is a pnj at (x,y,z), if so, add it to the mapping
	 * @param neighborhood neighborhood around a coordinate
	 * @param location relative location in string of the examined case from the caller
	 * @param x the coordinate x of the potential PNJ 
	 * @param y the coordinate y of the potential PNJ 
	 * @param z the coordinate z of the potential PNJ 
	 * @return map between the coordinate and the PNJ
	 */
	private HashMap<String, Creature> addPNJtoNeighborhood(HashMap<String, Creature> neighborhood, String location, int x, int y, int z) {//PNJ(x, y - 1, z));
		Creature PNJ = PNJ(x, y, z);
		if (PNJ != null) 
			neighborhood.put(location, PNJ);
		return neighborhood;
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @return the PNJ at certain coordinate
	 */
	private Creature PNJ(int x, int y, int z) {
		Creature creature = creature(x, y, z);
		if (creature != null && creature.isPNJ()) 
			return creature;
		return null;
			
	}
	
	/**
	 * Add a creature to the world
	 * @param creature creature needed to be added
	 */
	public void add(Creature creature) {
		creatures.add(creature);
	}
	
	
	public int width() { 
		return width; 
	}
	
	public int height() { 
		return height; 
	}

	public int depth() { 
		return depth; 
	}
}


