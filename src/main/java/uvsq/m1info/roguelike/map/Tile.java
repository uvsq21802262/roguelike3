package uvsq.m1info.roguelike.map;

import java.awt.Color;
import java.io.Serializable;

import asciiPanel.AsciiPanel;

public enum Tile implements Serializable {
	FLOOR((char)250, AsciiPanel.yellow, "A dirt and rock cave floor."),
	WALL1((char)178, AsciiPanel.yellow, "A dirt and rock cave wall."),
	WALL2((char)177, AsciiPanel.yellow, "A dirt and rock cave wall."),
	WALL3((char)176, AsciiPanel.yellow, "A dirt and rock cave wall."),
	WALLX('X', AsciiPanel.yellow, "A dirt and rock cave wall."),

	WATER((char)126, AsciiPanel.blue, "A submerged cave floor."),
	LAVA((char)126, Color.red, "Lava."),
	ICE((char)126, AsciiPanel.brightWhite, "Solid frozen floor."),
	POISON((char)126, AsciiPanel.brightGreen, "Poison"),
	SWAMP((char)126, AsciiPanel.green, "Swamp."),
	
	CHEST('C', AsciiPanel.yellow, "A treasure chest."),
	BOUNDS('x', AsciiPanel.brightBlack, "Beyond the edge of the world."),
	STAIRS_DOWN('>', AsciiPanel.white, "A stone staircase that goes down."),
	STAIRS_UP('<', AsciiPanel.white, "A stone staircase that goes up."),
	UNKNOWN(' ', AsciiPanel.white, "(unknown)");
	
	/**
	 * The character used for a tile
	 */
	private char glyph;
	public char glyph() { return glyph; }
	
	
	/**
	 * The color of a tile
	 */
	private Color color;
	public Color color() { return color; }

	/**
	 * The details of a tile
	 */
	private String description;
	public String details(){ return description; }
	
	/**
	 * Constructor not null
	 * @param glyph the character used for displaying a tile
	 * @param color the color used for displaying a tile
	 * @param description the description of the tile
	 */
	Tile(char glyph, Color color, String description){
		this.glyph = glyph;
		this.color = color;
		this.description = description;
	}

	
	/**
	 * @return if a tile is a ground, which includes floor, stairs, ice, water, lava, swamp and poison.
	 */
	public boolean isGround() {
		return this == FLOOR || this == STAIRS_DOWN || this == STAIRS_UP || this == ICE || this == WATER || this == LAVA || this == SWAMP || this == POISON;
	}
	
	/**
	 * @return if a tile is a floor without special effect
	 */
	public boolean isFloor() {
		return this == FLOOR;
	}

	/**
	 * @return true if a tile is a floor
	 * @return false if a tile is not a floor 
	 */
	public boolean isDiggable() {
		return this == FLOOR;
	}
	
	/**
	 * @return if a tile is one of the types of the walls
	 */
	public boolean isWall() {
		return this == WALL1 || this == WALL2 || this == WALL3 || this == WALLX;
	}
	
	//retourne le nombre de fois qu'il faut frapper le mur pour le casser
	/**
	 * Get the state of a wall
	 * @return the number of hits needed for digging a wall
	 */
	public int wallState() {
		switch (this) {
		case WALL1 : return 3;
		case WALL2 : return 2;
		case WALL3 : 
		case WALLX : return 1;
	default:
		break;
	}
	return 0;
	}
	
	/**
	 * @return true if a tile is a chest
	 */
	public boolean isChest(){
		return this == CHEST;
	}
	
	/**
	 * Determiner the type of a wall according to its number
	 * @param n the number specified for every type of the wall 
	 * @return the type of a tile that corresponds his type number
	 */
	public static Tile wallState(int n) {
		switch (n) {
			case 3 : return WALL1;
			case 2 : return WALL2;
			case 1 : return WALL3;
			default:
			return FLOOR;
		}
	}
}
