package uvsq.m1info.roguelike.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WorldBuilder implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3449261124681522150L;
	/**
	 * The width of the world
	 */
	private int width;
	/**
	 * The height of the world
	 */
	private int height;
	/**
	 * The depth of the world
	 */
	private int depth;
	/**
	 * The tiles of the world
	 */
	private Tile[][][] tiles;
	/**
	 * The regions of the world
	 */
	private int[][][] regions;
	/**
	 * The region is about to be built
	 */
	private int nextRegion;

	private boolean[][] cellmap = new boolean[width][height];
	private float chanceToStartAlive = 0.4f;
	private int birthLimit = 4;
	private int deathLimit = 3;
	private int numberOfSteps = 3;
	 
	/**
	 * Constructor of the world
	 * @param width width of the world
	 * @param height height of the world
	 * @param depth depth of the world
	 */
	public WorldBuilder(int width, int height, int depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.tiles = new Tile[width][height][depth];
		this.regions = new int[width][height][depth];
		this.nextRegion = 1;
	}

	/**
	 * Build the tiles of the world
	 * @return the world with tiles
	 */
	public World build() {
		return new World(tiles);
	}
	
	/**
	 * Determine the livable areas in a level 
	 * @param map area needed to be decided
	 * @return true if it is livable
	 *         false otherwise
	 */
	private boolean[][] initialiseMap(boolean[][] map){
	    for(int x=0; x<width; x++)
	        for(int y=0; y<height; y++)
                map[x][y] = Math.random() < chanceToStartAlive ? true : false;
	    return map;
	}
	
	
	/**
	 * Generate walls and floors
	 * @return world instance of worldbuilder
	 */
	public WorldBuilder generateMap(){
		for (int z = 0; z < depth; z++) {
		    cellmap = new boolean[width][height];
		    cellmap = initialiseMap(cellmap);
		    for (int i = 0; i < numberOfSteps; i++)
		        cellmap = doSimulationStep(cellmap);
		    
		    for (int x = 0; x < cellmap.length; x++)
		        for (int y = 0; y < cellmap[0].length; y++)
		        	tiles[x][y][z] = cellmap[x][y] ? randomWall():Tile.FLOOR;
		    placeTreasure(z);
		}
		return this;
	}
	
	
	/**
	 * Determine livable areas
	 * @param oldMap areas needed to be decided
	 * @return livable areas in a level
	 */
	private boolean[][] doSimulationStep(boolean[][] oldMap){
	    boolean[][] newMap = new boolean[width][height];
	    for (int x = 0; x < oldMap.length; x++)
	        for (int y = 0; y < oldMap[0].length; y++){
	            int nbs = countAliveNeighbours(oldMap, x, y);
	            //The new value is based on our simulation rules
	            //First, if a cell is alive but has too few neighbours, kill it.
	            if (oldMap[x][y])
	                if(nbs < deathLimit)
	                    newMap[x][y] = false;
	                else
	                    newMap[x][y] = true;
	            //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
	            else
	                if (nbs > birthLimit)
	                    newMap[x][y] = true;
	                else
	                    newMap[x][y] = false;
	        }
	    return newMap;
	}
	
	/**
	 * Calculate the number of neighbours 
	 * @param map areas needed to be decided
	 * @param x coordinate x of an area
	 * @param y coordinate y of an area
	 * @return the number of the livable neighbours around a coordinate
	 */
	private int countAliveNeighbours(boolean[][] map, int x, int y){
	    int count = 0;
	    for (int i = -1; i < 2; i++){
	        for (int j = -1; j < 2; j++){
	            int neighbour_x = x+i;
	            int neighbour_y = y+j;
	            if (i == 0 && j == 0)
	            	continue;
	            else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= map.length || neighbour_y >= map[0].length){
	                count = count + 1;
	            }
	            else if (map[neighbour_x][neighbour_y]){
	                count = count + 1;
	            }
	        }
	    }
	    return count;
	}

	private WorldBuilder createRegions() {
		regions = new int[width][height][depth];

		for (int z = 0; z < depth; z++) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (tiles[x][y][z].isWall() == false && regions[x][y][z] == 0) {
						int size = fillRegion(nextRegion++, x, y, z);

						if (size < 25)
							removeRegion(nextRegion - 1, z);
					}
				}
			}
		}
		return this;
	}

	private void removeRegion(int region, int z) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (regions[x][y][z] == region) {
					regions[x][y][z] = 0;
					tiles[x][y][z] = randomWall();
				}
			}
		}
	}

	private int fillRegion(int region, int x, int y, int z) {
		int size = 1;
		ArrayList<Point> open = new ArrayList<Point>();
		open.add(new Point(x, y, z));
		regions[x][y][z] = region;

		while (!open.isEmpty()) {
			Point p = open.remove(0);

			for (Point neighbor : p.neighbors8()) {
				if (neighbor.x < 0 || neighbor.y < 0 || neighbor.x >= width || neighbor.y >= height)
					continue;

				if (regions[neighbor.x][neighbor.y][neighbor.z] > 0
						|| tiles[neighbor.x][neighbor.y][neighbor.z] == randomWall())
					continue;

				size++;
				regions[neighbor.x][neighbor.y][neighbor.z] = region;
				open.add(neighbor);
			}
		}
		return size;
	}

	private WorldBuilder connectRegions() {
		for (int z = 0; z < depth - 1; z++) {
			connectRegionsDown(z);
		}
		return this;
	}

	private void connectRegionsDown(int z) {
		List<Integer> connected = new ArrayList<Integer>();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int r = regions[x][y][z] * 1000 + regions[x][y][z + 1];
				if (tiles[x][y][z] == Tile.FLOOR && tiles[x][y][z + 1] == Tile.FLOOR && !connected.contains(r)) {
					connected.add(r);
					connectRegionsDown(z, regions[x][y][z], regions[x][y][z + 1]);
				}
			}
		}
	}

	private void connectRegionsDown(int z, int r1, int r2) {
		List<Point> candidates = findRegionOverlaps(z, r1, r2);

		int stairs = 0;
		do {
			Point p = candidates.remove(0);
			tiles[p.x][p.y][z] = Tile.STAIRS_DOWN;
			tiles[p.x][p.y][z + 1] = Tile.STAIRS_UP;
			stairs++;
		} while (candidates.size() / stairs > 250);
	}

	public List<Point> findRegionOverlaps(int z, int r1, int r2) {
		ArrayList<Point> candidates = new ArrayList<Point>();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (tiles[x][y][z] == Tile.FLOOR && tiles[x][y][z + 1] == Tile.FLOOR && regions[x][y][z] == r1
						&& regions[x][y][z + 1] == r2) {
					candidates.add(new Point(x, y, z));
				}
			}
		}

		Collections.shuffle(candidates);
		return candidates;
	}
	
	

	/**
	 * Add stairs to the wolrd
	 * @return instance of worldbuilder
	 */
	private WorldBuilder addExitStairs() {
		int x = -1;
		int y = -1;

		do {
			x = (int)(Math.random() * width);
			y = (int)(Math.random() * height);
		} while (tiles[x][y][0] != Tile.FLOOR);

		tiles[x][y][0] = Tile.STAIRS_UP;
		return this;
	}

	/**
	 * Randomly get wall type 
	 * @return wall tile
	 */
	private Tile randomWall() {
		///*
		int rand = (int) (Math.random() * 1000);
		if (rand < 700)
			return Tile.WALL1;
		if (rand < 900)
			return Tile.WALL2;
		if (rand < 999)
			return Tile.WALL3;
		//*/
		return Tile.WALLX;
	}

	/**
	 * Generate livable areas, regions, stairs of the world
	 * @return instance of worldbuilder already built
	 */
	public WorldBuilder makeCaves() {
		return generateMap().createRegions().connectRegions().addExitStairs().createScenery();
	}
	
	/**
	 * Place treasure into the world
	 * @param depth the level to place the treasure
	 */
	private void placeTreasure(int depth) {
	    int treasureHiddenLimit = 6;
	    for (int x = 0; x < width; x++)
	        for (int y = 0; y < height; y++)
	            if (!cellmap[x][y]){
	                int nbs = countAliveNeighbours(cellmap, x, y);
	                if (nbs >= treasureHiddenLimit) 
	                    tiles[x][y][depth] = Tile.CHEST;
	            }
	}
	
	private WorldBuilder createScenery() {
		Tile tile = Tile.FLOOR;
		for (int k = 0; k < depth; k++){
			switch(k) {
				case(1) : tile = Tile.SWAMP; break;
				case(2) : tile = Tile.ICE; break;
				case(3) : tile = Tile.WATER; break;
				case(4) : tile = Tile.POISON; break;
			}
			for (int i = 0; i < width; i++) 
				for (int j = 0; j < height; j++) 
					if (tiles[i][j][k].isFloor() && Math.random() < 0.1) 
						tiles[i][j][k] = tile;
		}
		return this;
	}
}