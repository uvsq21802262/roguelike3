package uvsq.m1info.roguelike.map;


/**
 * This interface is used to set the coordinates of the objets.
 */


public interface Location {
	
  
    /**
     * @return Location of an object
     */
    public Location location();
    
    /**
     * Set the coordinates of an object with class location
     * @param Location location of an object
     */
    public void setLocation(Location location);
    
    
    /**
     * Set the coordinates of an object with x,y,z
     * @param x coordinate x of an object
     * @param y	coordinate y of an object
     * @param z coordinate z of an object
     */
    public void setLocation(int x, int y, int z);
    
    /**
     * @return coordinate x of an object 
     */
    public int getX();

    /**
     * @return coordinate y of an object 
     */
    public int getY();

    /**
     * @return coordinate z of an object 
     */
    public int getZ();
}
