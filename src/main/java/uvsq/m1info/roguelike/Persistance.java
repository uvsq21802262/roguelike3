package uvsq.m1info.roguelike;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import uvsq.m1info.roguelike.screens.PlayScreen;

public class Persistance{

	private static String  dirPath = "saves";
	private static String name = "saves"+File.separatorChar+"save";
	private static String  dirPathTest = "Test";
	private static String nameTest = "saves"+File.separatorChar+"Test";
	public static void save(PlayScreen playScreen) throws IOException {
		File dir = new File(dirPath);
		if(!dir.exists()) {
			dir.mkdirs();
			File fichier =  new File(name);
			try {
				fichier.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			execute(fichier,playScreen);
		}else if (dir.exists()) {
			File fichier =  new File(name);
			if(fichier.exists()){
				fichier.delete();
				fichier.createNewFile();
				execute(fichier,playScreen);
			}else {
				fichier.createNewFile();
				execute(fichier,playScreen);
			}
		}			
	}
	
	public static void saveTest(PlayScreen playScreen) throws IOException {
		File dir = new File(dirPathTest);
		if(!dir.exists()) {
			dir.mkdirs();
			File fichier =  new File(nameTest);
			try {
				fichier.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			execute(fichier,playScreen);
		}else if (dir.exists()) {
			File fichier =  new File(nameTest);
			if(fichier.exists()){
				fichier.delete();
				fichier.createNewFile();
				System.out.println("Sauvegarde existant");
				execute(fichier,playScreen);
			}else {
				fichier.createNewFile();
				execute(fichier,playScreen);
			}
		}			
	}

	public static PlayScreen resume() throws FileNotFoundException, IOException, ClassNotFoundException {
		File dir = new File(dirPath);
		if (dir.exists()) {
			File fichier =  new File(name);
			if (fichier.exists()) {	
				ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier)) ;
				PlayScreen p = (PlayScreen)ois.readObject() ;
				ois.close();
				return p;
			}
		}
	return null;
	}
	
	public static PlayScreen resumeTest() throws FileNotFoundException, IOException, ClassNotFoundException {
		File dir = new File(dirPathTest);
		if (dir.exists()) {
			File fichier =  new File(nameTest);
			if (fichier.exists()) {	
				ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier)) ;
				PlayScreen p = (PlayScreen)ois.readObject() ;
				ois.close();
				return p;
			}
		}
	return null;
	}
	
	private static void execute(File fichier, PlayScreen p) throws FileNotFoundException, IOException {
		ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichier)) ;
		oos.writeObject(p);
		oos.close();
	}
}

